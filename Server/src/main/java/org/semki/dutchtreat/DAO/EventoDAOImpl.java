package org.semki.dutchtreat.DAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.semki.dutchtreat.entity.Evento;
import org.semki.dutchtreat.utils.ObjectsSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class EventoDAOImpl extends BaseDAOImpl<Evento> implements EventoDAO {
	public EventoDAOImpl() {
		super(Evento.class);
	}
	
	public List<Evento> getRestrictedEventos(Long account_id)
	{
		Criteria cr = createCriteria();
		
		cr.createCriteria("accessAccounts","acc");
		
		cr.add(Restrictions.eq("acc.id", account_id));
		
		cr.add(Restrictions.eq("deleted",false));
				
		return ObjectsSupport.safeCastList(cr.list());
	}

	@Override
	public List<Evento> getAvailableList() {
		Criteria cr = createCriteria();
		
		cr.add(Restrictions.eq("deleted", false));		
				
		return ObjectsSupport.safeCastList(cr.list());
	}

	public void markDeleted(Long eveniId)
	{
		Evento evento = get(eveniId);
		markDeleted(evento);
	}
	
	public void markDeleted(Evento evento)
	{
		evento.setDeleted(true);
		update(evento);
	}
}