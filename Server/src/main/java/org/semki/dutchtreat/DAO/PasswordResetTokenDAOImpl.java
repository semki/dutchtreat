package org.semki.dutchtreat.DAO;

import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.semki.dutchtreat.entity.Account;
import org.semki.dutchtreat.entity.PasswordResetToken;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class PasswordResetTokenDAOImpl extends BaseDAOImpl<PasswordResetToken> implements PasswordResetTokenDAO {

	public PasswordResetTokenDAOImpl() {
		super(PasswordResetToken.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public PasswordResetToken getTokenByKey(String key) {
		return this.first(createCriteria().add(Restrictions.eq("key", key))
				.list());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PasswordResetToken> getTokensByAccount(Account account) {
		return this.createCriteria().add(Restrictions.eq("account.id", account.getId()))
				.list();
	}

}
