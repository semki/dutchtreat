package org.semki.dutchtreat.DAO;

import java.util.List;

import org.semki.dutchtreat.entity.Account;
import org.semki.dutchtreat.entity.PasswordResetToken;

public interface PasswordResetTokenDAO extends BaseDAO<PasswordResetToken>  {
	
	PasswordResetToken getTokenByKey(String key);
	
	List<PasswordResetToken> getTokensByAccount(Account account);
}
