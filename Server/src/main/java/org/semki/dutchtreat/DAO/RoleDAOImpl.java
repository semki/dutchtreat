package org.semki.dutchtreat.DAO;

import org.hibernate.criterion.Restrictions;
import org.semki.dutchtreat.entity.Role;
import org.semki.dutchtreat.utils.ObjectsSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class RoleDAOImpl extends BaseDAOImpl<Role> implements RoleDAO {
	
	public RoleDAOImpl() {
		super(Role.class);
	}

	@Override
	public Role getRoleByName(String name) {
		return first(ObjectsSupport.safeCastList(createCriteria().add(Restrictions.eq("name", name)).list()));
	}

}
