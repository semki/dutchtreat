package org.semki.dutchtreat.mvc.models;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.velocity.app.VelocityEngine;
import org.semki.dutchtreat.DAO.PasswordResetTokenDAO;
import org.semki.dutchtreat.core.exceptions.UserException;
import org.semki.dutchtreat.entity.Account;
import org.semki.dutchtreat.entity.PasswordResetToken;
import org.semki.dutchtreat.utils.UserUrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.util.UriComponentsBuilder;

@Transactional
@Service
public class PasswordResetTokenModel {

	@Autowired
	private VelocityEngine velocityEngine;

	@Autowired
	private PasswordResetTokenDAO tokenDaoImpl;

	public PasswordResetToken createByAccount(Account account) {
		
		List<PasswordResetToken> oldTokenList = tokenDaoImpl.getTokensByAccount(account);
		
		for (PasswordResetToken token : oldTokenList) {
			token.setAvailable(false);
			tokenDaoImpl.saveOrUpdate(token);
		}
		
		PasswordResetToken token = new PasswordResetToken();
		token.setKey(UUID.randomUUID().toString());
		token.setAccount(account);
		token.setExpireDate(new java.sql.Date(new java.util.Date().getTime()));
		token.setAvailable(true);

		tokenDaoImpl.save(token);

		return token;
	}

	public String getPasswordResetMessage(PasswordResetToken token) {
		String messageText = "";
		try {
			Map<String, Object> model = new HashMap<String, Object>();

			model.put("reset_url", getResetPasswordUrl(token));

			model.put("username", token.getAccount().getName());
			
			model.put("adminemail", "admin@de1mos.net");

			messageText = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
					"velocity/resetPassword.vm", "UTF-8", model);
		} catch (Throwable e) {
			throw new UserException(
					"Ошибка при формировании Email c инструкциями");
		}

		return messageText;
	}

	private String getResetPasswordUrl(PasswordResetToken token)
			throws URISyntaxException {

		UserUrlUtils url = new UserUrlUtils();
				
		String queryString = String.format("key=%s",token.getKey());

		return UriComponentsBuilder.fromUri(url.getBaseUrl()).path("/app/auth/changepassword").query(queryString).toUriString();
	}

	public Account getAccountByTokenKey(String key) {
		PasswordResetToken token = tokenDaoImpl.getTokenByKey(key);
		
		if ((token==null) || (!token.getAvailable()))
		{
			throw new UserException(String.format("ключ %s не актуален повторите сброс пароля заново", key));
		}
		
		token.setAvailable(false);
		
		tokenDaoImpl.save(token);
		
		return token.getAccount();
	}
}
