package org.semki.dutchtreat.mvc.dto;

public class ChangePasswordDTO {
	public String key;
	public String password;
	public String password_confirmation;
}
