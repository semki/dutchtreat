package org.semki.dutchtreat.mvc.dto;

import org.semki.dutchtreat.entity.Account;
import org.semki.dutchtreat.entity.Participant;

public class ParticipantDTO {
	
	public Long id;
	
	public String name;
	
	public Long event_id;
	
	public AccountDTO linkedAccount;
	
	public Participant convertToEntity() {
		Participant entity = new Participant();
		entity.setId(id);
		entity.setName(name);
		entity.setDeleted(false);
		return entity;
	}

	public static ParticipantDTO convertToDTO(Participant participant) {
		ParticipantDTO dto = new ParticipantDTO();
		dto.id = participant.getId();
		dto.name = participant.getName();
		dto.event_id = participant.getEvento().getId();
		
		Account linkedAccount = participant.getLinkedAccount();
		if (linkedAccount != null) {
			dto.linkedAccount = AccountDTO.convertToTransport(linkedAccount);
		}
		return dto;
	}
	
	public static Participant createByName(String name) {
		Participant entity = new Participant();
		entity.setId(null);
		entity.setName(name);
		entity.setDeleted(false);
		return entity;
	}
}
