package org.semki.dutchtreat.core.initiaze;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

public class SecurityInit implements InitializingBean{
	
	@Autowired
	private SecurityInitImpl securityInitImpl;
	
	static Logger log = Logger.getLogger(SecurityInit.class);
	

	@Override
	public void afterPropertiesSet() throws Exception {
		
		securityInitImpl.runInit();
	}

}
