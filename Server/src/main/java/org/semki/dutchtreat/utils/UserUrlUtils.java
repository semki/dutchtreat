package org.semki.dutchtreat.utils;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class UserUrlUtils {
	
	private HttpServletRequest request;

	public UserUrlUtils() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if(null != requestAttributes && requestAttributes instanceof ServletRequestAttributes) {
		  request = ((ServletRequestAttributes)requestAttributes).getRequest(); 
		}
		
	}
	
	public URI getBaseUrl() throws URISyntaxException
	{
		URI requestUri = new URI(request.getRequestURL().toString());
		
		URI contextUri = new URI(requestUri.getScheme(), 
		                         requestUri.getAuthority(), 
		                         request.getContextPath(), 
		                         null, 
		                         null);
		return contextUri;
	}
}
