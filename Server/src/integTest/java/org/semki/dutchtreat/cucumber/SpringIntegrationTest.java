package org.semki.dutchtreat.cucumber;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by denis on 20.10.16.
 */
@ContextConfiguration("classpath:/config/spring/root-context.xml")
@ActiveProfiles("test")
public class SpringIntegrationTest {
}
