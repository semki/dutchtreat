package org.semki.dutchtreat.cucumber.steps;

import cucumber.api.PendingException;
import cucumber.api.java.ru.*;
import org.junit.Assert;
import org.semki.dutchtreat.DAO.AccountDAO;
import org.semki.dutchtreat.config.security.AccountUserDetails;
import org.semki.dutchtreat.cucumber.CucumberContext;
import org.semki.dutchtreat.cucumber.SpringIntegrationTest;
import org.semki.dutchtreat.entity.Account;
import org.semki.dutchtreat.mvc.controllers.AccountController;
import org.semki.dutchtreat.mvc.controllers.EventsController;
import org.semki.dutchtreat.mvc.dto.AccountDTO;
import org.semki.dutchtreat.mvc.dto.EventDTO;
import org.semki.dutchtreat.mvc.models.EventModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

/**
 * Created by denis on 20.10.16.
 */
public class EventsStepDefs extends SpringIntegrationTest {

    @Autowired
    AccountController accountController;
    @Autowired
    AccountDAO accountDAO;
    @Autowired
    EventsController eventsController;
    @Autowired
    private EventModel eventModel;
    @Autowired
    private BCryptPasswordEncoder passEncoder;

    @Autowired
    CucumberContext context;

    @Когда("^пользователь создает событие с названием \"([^\"]*)\"$")
    public void пользовательСоздаетСобытиеСНазванием(String eventName) throws Throwable {
        EventDTO dto = new EventDTO();
        dto.name = eventName;
        eventModel.createEvent(dto);
    }

    @То("^в списке событий он должен увидеть (\\d+) событие с названием \"([^\"]*)\"$")
    public void вСпискеСобытийОнДолженУвидетьСобытиеСНазванием(Long expectedCount, String eventName) throws Throwable {
        List<EventDTO> eventDTOs = eventsController.getEvents();
        Long actualCount = eventDTOs.stream().filter(eventDTO -> eventDTO.name.equals(eventName)).count();
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Дано("^в системе должен быть зарегистрирован пользователь \"([^\"]*)\" с паролем \"([^\"]*)\"$")
    public void вСистемеДолженБытьЗарегистрированПользовательСПаролем(String login, String password) throws Throwable {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.user_login = login;
        accountDTO.user_password = password;
        accountDTO.password_confirmation = password;
        accountDTO.active = true;
        accountController.createAccount(accountDTO);
    }

    @И("^открыв на редактирование карточку события \"([^\"]*)\"$")
    public void открывНаРедактированиеКарточкуСобытия(String eventName) throws Throwable {
        context.eventDTO = eventsController.getEvents().stream().
                filter(eventDTO -> eventDTO.name.equals(eventName)).findFirst().get();
    }

    @Тогда("^в карточке редактирования события в списке доступа должен быть пользователь \"([^\"]*)\"$")
    public void вКарточкеРедактированияСобытияВСпискеДоступаДолженБытьПользователь(String login) throws Throwable {
        boolean userFound = context.eventDTO.accessAccounts.stream()
                .anyMatch(accountDTO -> accountDTO.user_login.equals(login));
        Assert.assertTrue(userFound);
    }

    @Пусть("^пользователь \"([^\"]*)\" совершил вход$")
    public void пользовательСовершилВход(String login) throws Throwable {
        Account user = accountDAO.getAccountByName(login);
        AccountUserDetails userDetails = new AccountUserDetails(user);
        TestingAuthenticationToken authenticationToken = new TestingAuthenticationToken(userDetails, null);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }

    @И("^Укаываем в карточке навание \"([^\"]*)\"$")
    public void укаываемВКарточкеНавание(String arg0) throws Throwable {
        context.eventDTO.name = arg0;
    }

    @И("^Сохраняем карточку редактирования события$")
    public void сохраняемКарточкуРедактированияСобытия() throws Throwable {
        eventsController.update(context.eventDTO, context.eventDTO.id);
    }
}
