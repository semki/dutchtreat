package org.semki.dutchtreat.cucumber;

import org.semki.dutchtreat.mvc.dto.EventDTO;
import org.springframework.stereotype.Component;

/**
 * Created by denis on 20.10.16.
 */
@Component
public class CucumberContext {

    public EventDTO eventDTO;
}
