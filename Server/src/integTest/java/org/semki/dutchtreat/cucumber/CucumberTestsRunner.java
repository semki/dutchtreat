package org.semki.dutchtreat.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by denis on 19.10.16.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features/",
        glue = {"org.semki.dutchtreat.cucumber.steps", "cucumber.api.spring"},
        plugin = {"pretty", "html:build/cucumber", "json:build/cucumber.json"},
        monochrome = true)
public class CucumberTestsRunner {

}