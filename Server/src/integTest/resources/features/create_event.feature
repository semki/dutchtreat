# language: ru
Функционал: У пользователя должна быть возможность создавать события

  Предыстория:
    Дано в системе должен быть зарегистрирован пользователь "Тестовый пользователь" с паролем "1234"
    Пусть пользователь "Тестовый пользователь" совершил вход

  @txn
  Сценарий: Создание пустого события

    Когда пользователь создает событие с названием "Тестовое событие"
    То в списке событий он должен увидеть 1 событие с названием "Тестовое событие"

  @txn
  Сценарий: Создание дубликата события (Когда-нибудь мы это поправим)
    Когда пользователь создает событие с названием "Тестовое событие"
    То в списке событий он должен увидеть 1 событие с названием "Тестовое событие"
    Когда пользователь создает событие с названием "Тестовое событие"
    То в списке событий он должен увидеть 2 событие с названием "Тестовое событие"

  @txn
  Сценарий: При создании события пользователь должен быть привязан к событию
    Когда пользователь создает событие с названием "Тестовое событие"
    То в списке событий он должен увидеть 1 событие с названием "Тестовое событие"
    И открыв на редактирование карточку события "Тестовое событие"
    Тогда в карточке редактирования события в списке доступа должен быть пользователь "Тестовый пользователь"

  @txn
  Сценарий: После редактирования события пользователь должен увидеть измененные данные
    Когда пользователь создает событие с названием "Тестовое событие"
    То в списке событий он должен увидеть 1 событие с названием "Тестовое событие"
    И открыв на редактирование карточку события "Тестовое событие"
    И Укаываем в карточке навание "Событие митап"
    И Сохраняем карточку редактирования события
    То в списке событий он должен увидеть 0 событие с названием "Тестовое событие"
    То в списке событий он должен увидеть 1 событие с названием "Событие митап"