'use strict';

var webpack = require('webpack');

/**
 * Production config
 * @param  {String} _path Absolute path to application
 * @return {Object}       Object of proruction settings
 */
module.exports = function(_path) {
  return {
    context: _path,
    debug: false,
    devtool: 'cheap-source-map',
    output: {
      publicPath: '/'
    },
    plugins: [
		new webpack.optimize.UglifyJsPlugin({
                compress: {
                  // don't show unreachable variables etc
                  warnings:     false,
                  drop_console: true,
                  unsafe:       true
              }
        }),
    ]
  }
}