'use strict';  

var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var BowerWebpackPlugin = require('bower-webpack-plugin');

module.exports = function(_path){
    return {

        context: path.join(_path, '/app'),
        resolve: {
            modulesDirectories: ["node_modules", "bower_components"],
            root: [path.resolve(_path, 'app'), path.resolve(_path, 'node_modules')],
            extensions: ['', '.js']
        },
        entry: {
            app: _path + '/app',
            vendor: [
                    'angular/angular-csp.css',
                    'angular',
                    'angular-animate',
                    'angular-loader',
                    'angular-loading-bar',
                    'angular-route',
                    'angular-aria',
                    'angular-material',
                    'angular-resource',
                    'moment',
                    'angular-messages',
                    'angular-material-data-table'
            ]
        },
        output: {
            path: _path + '/build',
            filename: '[name].[chunkhash:6].js',
            publicPath: './'
        },
        plugins: [
            new webpack.NoErrorsPlugin(),
            new BowerWebpackPlugin({
                modulesDirectories: ["bower_components"],
                manifestFiles:      "bower.json",
                includes:           /.*/,
                excludes:           [],
                searchResolveModulesDirectories: true
            }),
            new HtmlWebpackPlugin({
                filename: _path + '/build/index.html',
                template: path.join(_path, 'app', 'index.html'),
                favicon: path.join(_path, 'app', 'favicon.ico')
            }),
            new ExtractTextPlugin('[name].css', {allChunks: true}),
            new webpack.ContextReplacementPlugin( /bower_components\/moment\/locale/, /ru|en-gb/)
        ],
        
        module: {
            loaders: [
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract('style-loader', 'css')
                },
                {
                    test:   /\.html$/,
                    exclude: /index\.html/,
                    loader: 'file?name=[path][name].[hash:6].[ext]'
                },
                {
                    test:   /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                    loader: 'url?limit=10000&name=[path][name].[hash:6].[ext]'
                }
            ],

            noParse: /\/node_modules\/(angular\/angular)/,

            preLoaders: [
                {
                    test: /\.js$/,
                    loaders: ['jshint'],
                    // define an include so we check just the files we need
                    exclude: [/bower_components/, /node_modules/],

                }
            ]
        },

        jshint: {
            emitErrors: true,
            failOnHint: true
        }
    }
};
