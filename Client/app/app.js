"use strict";

angular.module("Utils",["ngMaterial"]);
angular.module("Eventos", ["Participants", "ngRoute", "Purchases", "ngResource","Authentication", "Utils"]);
angular.module("Purchases", ["Participants", "Utils"]);
angular.module("Transfers", ["Participants", "Utils"]);
angular.module("Balance", ["Eventos", "Utils"]);
angular.module("Authentication",["Utils"]);
angular.module("Admin",["Utils"]);


var CHANGE_PASSWORD_URL = "/auth/changepassword";
var LOGIN_URL = "/auth/login";


// Declare app level module which depends on views, and components
angular.module("semki.DutchTreat", [
  "ngAnimate", "ngRoute","ngResource", "ui.bootstrap", "ngTagsInput", "ngMessages",
  "angular-loading-bar", "isteven-multi-select","angular-growl", "ngMaterial",
  "md.data.table",
  "Eventos", "Purchases", "Transfers", "Balance","Authentication","Admin", "Utils"])

  .config(function($locationProvider, $routeProvider, $httpProvider,growlProvider) {

    $locationProvider.html5Mode(true);

    growlProvider.globalTimeToLive(5000);
    growlProvider.globalPosition('bottom-center');

    $routeProvider
      .when("/admin", {
        templateUrl: "views/admin/md-admin-view.html",
        controller: "AdminUserListCtrl"
      })
      .when("/events", {
        templateUrl: "views/events/md-events-list.html",
        controller: "EventosCtrl"
      })
      .when("/events/:id/edit", {
      	templateUrl: "views/events/md-event-edit.html",
        controller: "EventoEditCtrl"
      })
      .when("/events/:id/addUser/:invateHash", {
        templateUrl: "views/events/md-event-view.html",
        controller: "EventoUrlEditCtrl"
      })
      .when("/events/new", {
      	templateUrl: "views/events/md-event-new.html",
      	controller: "EventoNewCtrl"
      })
      .when("/events/:event_id/purchases/new", {
      	templateUrl: "views/purchases/md-purchase-new.html",
      	controller: "PurchaseNewCtrl"
      })
      .when("/events/:event_id/purchases/:id/edit", {
      	templateUrl: "views/purchases/md-purchase-edit.html",
      	controller: "PurchaseEditCtrl"
      })
       .when("/events/:event_id/purchases/:id/createCopy", {
        templateUrl: "views/purchases/md-purchase-new.html",
        controller: "PurchaseCreateCopyCtrl"
      })
      .when("/events/:id", {
      	templateUrl: "views/events/md-event-view.html",
        controller: "EventoViewCtrl"
      })
      .when("/events/:event_id/transfers", {
      	templateUrl: "views/transfers/md-transfers-list.html",
      	controller: "TransfersCtrl"
      })
      .when("/events/:event_id/transfers/new", {
      	templateUrl: "views/transfers/md-transfer-new.html",
      	controller: "TransferNewCtrl"
      })
      .when("/events/:event_id/transfers/:id/edit", {
      	templateUrl: "views/transfers/md-transfer-edit.html",
      	controller: "TransferEditCtrl"
      })
      .when("/events/:event_id/transfers/:id/createCopy", {
        templateUrl: "views/transfers/md-transfer-new.html",
        controller: "TransferCreateCopyCtrl"
      })
      .when("/events/:event_id/balance", {
        templateUrl: "views/balance/md-pariticipant-balance-view.html",
        controller: "ParticipantBalanceCtrl"
      })
      .when("/events/:event_id/balance/:participant_id", {
        templateUrl: "views/balance/md-pariticipant-balance-view.html",
        controller: "ParticipantBalanceCtrl"
      })
      .when("/auth/login", {
        templateUrl: "views/auth/md-signin.html",
        controller: "AuthenticationCtrl"
      })
      .when("/auth/registration", {
        templateUrl: "views/auth/md-account-new.html",
        controller: "AccountNewCtrl"
      })
      .when("/auth/resetpassword", {
        templateUrl: "views/auth/md-reset-password.html",
        controller: "AccountResetPasswordCtrl"
      })
      .when("/auth/changepassword", {
        templateUrl: "views/auth/md-change-password.html",
        controller: "AccountChangePasswordCtrl"
      })
      .when("/auth/account/:account_id/edit", {
        templateUrl: "views/auth/md-account-edit.html",
        controller: "AccountEditCtrl"
      })
      .otherwise({
        redirectTo: "events/"
      });

    var interceptor = function ($q,$location,$rootScope,ToastService) {
      return {
        request: function ( config ) { 
            //console.log("run request");
            return config;
          },
        response: function ( response ) {
            //console.log("ger response");
            return response;
          },
        responseError: function ( response ) { 
           if (response.status === 401) {
                //console.log("Response 401");

                if ($rootScope.previousPage == null) {
                  $rootScope.previousPage = $location.$$path;
                  //console.log("previousPage = " + $rootScope.previousPage);
                }
                else {
                  //console.log("use old previousPage = " + $rootScope.previousPage); 
                }

                var urlList = [CHANGE_PASSWORD_URL,LOGIN_URL];

                if (urlList.indexOf($location.$$path) == -1)
                {
                  $location.url(LOGIN_URL);
                }
            }
            else if(response.status === 403)
            {
              ToastService.show('доступ запрещен');
              $location.url('/auth/events'); 
            }
            else if((response.status >= 500)&&(response.status < 600))
            {
              console.log("ger error");
              ToastService.show("На сервере произошла ошибка");
            }
            else
            {
              // other errors
            }
            return $q.reject( response );
          }
        };
      };
    $httpProvider.interceptors.push(interceptor);
  }
)

.config(function($mdThemingProvider, $mdIconProvider){
                  //https://design.google.com/icons/
                  $mdIconProvider
                      .defaultIconSet("./assets/svg/avatars.svg", 128)
                      .icon("menu"       , "./assets/svg/menu.svg"        , 24)
                      .icon("share"      , "./assets/svg/share.svg"       , 24)
                      .icon("google_plus", "./assets/svg/google_plus.svg" , 512)
                      .icon("hangouts"   , "./assets/svg/hangouts.svg"    , 512)
                      .icon("twitter"    , "./assets/svg/twitter.svg"     , 512)
                      .icon("phone"      , "./assets/svg/phone.svg"       , 512)

                      .icon("delete_item", "./assets/svg/ic_delete_black_18px.svg", 18)
                      .icon("edit_item", "./assets/svg/ic_mode_edit_black_18px.svg", 18)
                      .icon("add_item", "./assets/svg/ic_add_circle_outline_black_18px.svg", 18)
                      .icon("add_item24", "./assets/svg/ic_add_black_24px.svg", 18)
                      .icon("copy_item", "./assets/svg/ic_content_copy_black_18px.svg", 18)
                      .icon("login", "./assets/svg/login.svg", 24)
                      .icon("logout", "./assets/svg/logout.svg", 24)
                      .icon("settings", "./assets/svg/ic_build_black_24px.svg", 24)
                      .icon("assignment", "./assets/svg/ic_assignment_ind_black_18px.svg", 18)

                      ;

                      $mdThemingProvider.theme('default')
                          .primaryPalette('green')
                          .accentPalette('lime')
                          ;

                      $mdThemingProvider.theme('toast-theme')
                          .primaryPalette('green')
                          .accentPalette('lime');

                      $mdThemingProvider.setDefaultTheme('default');
                  }
)

.config(function($mdDateLocaleProvider) {
  // Example of a French localization.
  $mdDateLocaleProvider.months = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
  $mdDateLocaleProvider.shortMonths = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
  $mdDateLocaleProvider.days = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];
  $mdDateLocaleProvider.shortDays = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
  // Can change week display to start on Monday.
  $mdDateLocaleProvider.firstDayOfWeek = 1;
  // Optional.
  //$mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6, ...];

  // Example uses moment.js to parse and format dates.
  $mdDateLocaleProvider.parseDate = function(dateString) {
    var m = moment(dateString, 'L', true);
    return m.isValid() ? m.toDate() : new Date(NaN);
  };
  $mdDateLocaleProvider.formatDate = function(date) {
    return moment(date).format('L');
  };
  $mdDateLocaleProvider.monthHeaderFormatter = function(date) {
    return moment().month(date.getMonth()).year(date.getFullYear()).format('MMMM YYYY');
  };
  // In addition to date display, date components also need localized messages
  // for aria-labels for screen-reader users.
  $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
    return 'Неделя ' + weekNumber;
  };
  $mdDateLocaleProvider.msgCalendar = 'Календарь';
  $mdDateLocaleProvider.msgOpenCalendar = 'Открыть календарь';
});
;
