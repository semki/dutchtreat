"use strict";

var CHANGE_PASSWORD_URL = "/auth/changepassword";
var LOGIN_URL = "/auth/login";

var httpInterceptor = ["$q","$location","$rootScope","ToastService",
  function ($q,$location,$rootScope,ToastService) {
      return {
        request: function ( config ) { 
            //console.log("run request");
            return config;
          },
        response: function ( response ) {
            //console.log("ger response");
            return response;
          },
        responseError: function ( response ) { 
          //return;
          console.log("Response with error: " + response);
           if (response.status === 401) {
                console.log("Response 401");

                if (typeof($rootScope.previousPage) === "undefined" || $rootScope.previousPage === null) {
                  $rootScope.previousPage = $location.$$path;
                  console.log("previousPage = " + $rootScope.previousPage);
                }
                else {
                  console.log("use old previousPage = " + $rootScope.previousPage); 
                }

                var urlList = [CHANGE_PASSWORD_URL,LOGIN_URL];

                console.log("Response 401 goto " + urlList);

                if (urlList.indexOf($location.$$path) == -1)
                {
                  console.log("Response 401 lets go " + LOGIN_URL);
                  $location.url(LOGIN_URL);
                }
            }
            else if(response.status === 403)
            {
              ToastService.show('доступ запрещен');
              $location.url('/auth/events'); 
            }
            else if((response.status >= 500)&&(response.status < 600))
            {
              console.log("ger error");
              ToastService.show("На сервере произошла ошибка");
            }
            else
            {
              // other errors
            }
            return $q.reject( response );
          }
        };
      }];

module.exports = ["$httpProvider",
  function ($httpProvider) {
    $httpProvider.interceptors.push(httpInterceptor);
}];

