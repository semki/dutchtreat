"use strict";

module.exports = ["$mdThemingProvider", "$mdIconProvider",
    function($mdThemingProvider, $mdIconProvider){
        //https://design.google.com/icons/
        $mdIconProvider
            .icon("menu"       , require('assets/svg/menu.svg')        , 24)
            .icon("share"      , require('assets/svg/share.svg')       , 24)
            .icon("google_plus", require('assets/svg/google_plus.svg') , 512)
            .icon("hangouts"   , require('assets/svg/hangouts.svg')    , 512)
            .icon("twitter"    , require('assets/svg/twitter.svg')     , 512)
            .icon("phone"      , require('assets/svg/phone.svg')       , 512)

            .icon("delete_item", require('assets/svg/ic_delete_black_18px.svg'), 18)
            .icon("edit_item", require('assets/svg/ic_mode_edit_black_18px.svg'), 18)
            .icon("add_item", require('assets/svg/ic_add_circle_outline_black_18px.svg'), 18)
            .icon("add_item24", require('assets/svg/ic_add_black_24px.svg'), 18)
            .icon("copy_item", require('assets/svg/ic_content_copy_black_18px.svg'), 18)
            .icon("login", require('assets/svg/login.svg'), 24)
            .icon("logout", require('assets/svg/logout.svg'), 24)
            .icon("settings", require('assets/svg/ic_build_black_24px.svg'), 24)
            .icon("assignment", require('assets/svg/ic_assignment_ind_black_18px.svg'), 18)
            .icon("clear", require('assets/svg/ic_clear_black_24px.svg'), 18)
            ;

        $mdThemingProvider.theme('default')
            .primaryPalette('green')
            .accentPalette('lime')
            ;

        $mdThemingProvider.theme('toast-theme')
            .primaryPalette('green')
            .accentPalette('lime');

        $mdThemingProvider.setDefaultTheme('default');
}];
