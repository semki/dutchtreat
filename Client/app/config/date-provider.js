"use strict";

module.exports = ["$mdDateLocaleProvider",
  function($mdDateLocaleProvider) {
    var moment = require('moment');

    // Example of a French localization.
    $mdDateLocaleProvider.months = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
    $mdDateLocaleProvider.shortMonths = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
    $mdDateLocaleProvider.days = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];
    $mdDateLocaleProvider.shortDays = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
    // Can change week display to start on Monday.
    $mdDateLocaleProvider.firstDayOfWeek = 1;
    // Optional.
    //$mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6, ...];

    // Example uses moment.js to parse and format dates.
    $mdDateLocaleProvider.parseDate = function(dateString) {
      var m = moment(dateString, 'L', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
    };
    $mdDateLocaleProvider.formatDate = function(date) {
      return moment(date).format('L');
    };
    $mdDateLocaleProvider.monthHeaderFormatter = function(date) {
      return moment().month(date.getMonth()).year(date.getFullYear()).format('MMMM YYYY');
    };
    // In addition to date display, date components also need localized messages
    // for aria-labels for screen-reader users.
    $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
      return 'Неделя ' + weekNumber;
    };
    $mdDateLocaleProvider.msgCalendar = 'Календарь';
    $mdDateLocaleProvider.msgOpenCalendar = 'Открыть календарь';
}];
