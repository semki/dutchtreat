"use strict";

module.exports = ["$locationProvider", "$routeProvider", "$httpProvider",
  function($locationProvider, $routeProvider, $httpProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider
      .when("/admin", {
        templateUrl: require('views/admin/md-admin-view.html'),
        controller: "AdminUserListCtrl"
      })
      .when("/events", {
        templateUrl: require('views/events/md-events-list.html'),
        controller: "EventosCtrl"
      })
      .when("/events/:id/edit", {
      	templateUrl: require('views/events/md-event-edit.html'),
        controller: "EventoEditCtrl"
      })
      .when("/events/:id/link-participants", {
        templateUrl: require('views/events/md-eveent-link-participants.html'),
        controller: "EventoParticipantsLinkCtrl"
      })
      .when("/events/:id/addUser/:invateHash", {
        templateUrl: require('views/events/md-event-view.html'),
        controller: "EventoUrlEditCtrl"
      })
      .when("/events/new", {
      	templateUrl: require('views/events/md-event-new.html'),
      	controller: "EventoNewCtrl"
      })
      .when("/events/:event_id/purchases/new", {
      	templateUrl: require('views/purchases/md-purchase-new.html'),
      	controller: "PurchaseNewCtrl"
      })
      .when("/events/:event_id/purchases/:id/edit", {
      	templateUrl: require('views/purchases/md-purchase-edit.html'),
      	controller: "PurchaseEditCtrl"
      })
       .when("/events/:event_id/purchases/:id/createCopy", {
        templateUrl: require('views/purchases/md-purchase-new.html'),
        controller: "PurchaseCreateCopyCtrl"
      })
      .when("/events/:id", {
      	templateUrl: require('views/events/md-event-view.html'),
        controller: "EventoViewCtrl"
      })
      .when("/events/:event_id/transfers", {
      	templateUrl: require('views/transfers/md-transfers-list.html'),
      	controller: "TransfersCtrl"
      })
      .when("/events/:event_id/transfers/new", {
      	templateUrl: require('views/transfers/md-transfer-new.html'),
      	controller: "TransferNewCtrl"
      })
      .when("/events/:event_id/transfers/:id/edit", {
      	templateUrl: require('views/transfers/md-transfer-edit.html'),
      	controller: "TransferEditCtrl"
      })
      .when("/events/:event_id/transfers/:id/createCopy", {
        templateUrl: require('views/transfers/md-transfer-new.html'),
        controller: "TransferCreateCopyCtrl"
      })
      .when("/events/:event_id/balance", {
        templateUrl: require('views/balance/md-pariticipant-balance-view.html'),
        controller: "ParticipantBalanceCtrl"
      })
      .when("/events/:event_id/balance/:participant_id", {
        templateUrl: require('views/balance/md-pariticipant-balance-view.html'),
        controller: "ParticipantBalanceCtrl"
      })
      .when("/auth/login", {
        templateUrl: require('views/auth/md-signin.html'),
        controller: "AuthenticationCtrl"
      })
      .when("/auth/registration", {
        templateUrl: require('views/auth/md-account-new.html'),
        controller: "AccountNewCtrl"
      })
      .when("/auth/resetpassword", {
        templateUrl: require('views/auth/md-reset-password.html'),
        controller: "AccountResetPasswordCtrl"
      })
      .when("/auth/changepassword", {
        templateUrl: require('views/auth/md-change-password.html'),
        controller: "AccountChangePasswordCtrl"
      })
      .when("/auth/account/:account_id/edit", {
        templateUrl: require('views/auth/md-account-edit.html'),
        controller: "AccountEditCtrl"
      })
      .otherwise({
        redirectTo: "events/"
      });
}];
