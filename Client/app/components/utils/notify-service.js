"use strict";

module.exports = ["$rootScope",
    function ($rootScope) {
        return {
            subscribe: function(scope, eventName, callback) {
                var handler = $rootScope.$on(eventName, callback);
                scope.$on('$destroy', handler);
            },

            // eventData = {} by default
            notify: function(eventName, eventData) {
            	eventData = typeof eventData !== 'undefined' ? eventData : {};
                $rootScope.$emit(eventName, eventData);
            }
        };
}];