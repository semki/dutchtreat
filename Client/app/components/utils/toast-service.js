"use strict";

module.exports = ["$injector", 
	function ($injector) {
		var messagesStack = [];

		var getMdToast = function() {
			return $injector.get('$mdToast');
		};

		var onHidePrevious = function(showResult) {
					var mdToast = getMdToast();
					mdToast.hide();
					messagesStack.shift();
					showNextToast();
				};

		var showNextToast = function () {
			var toast = messagesStack[0];
			if (typeof(toast) !== "undefined" && toast !== null) {
				var mdToast = getMdToast();
				mdToast.show(toast).then(onHidePrevious, onHidePrevious);
			}
		};

		var addToStack = function(toast) {
			if (messagesStack.push(toast) == 1)
			{
				showNextToast();
			}
		};

		return {
			show : function (text) {
				var mdToast = getMdToast();
				var toast = mdToast.simple()
	                  .action(text)
	                  .highlightAction(true)
	  				  .highlightClass('md-primary')
	                  .position('top right')
	                  .hideDelay(2000);
		        addToStack(toast);
				},
			warning: function (text) {
				var mdToast = getMdToast();
				var toast = mdToast.simple()
	                  .action(text)
	                  .highlightAction(true)
	  				  .highlightClass('md-warn')
	                  .position('top right')
	                  .hideDelay(2000);
		        addToStack(toast);
				}
		};
}];