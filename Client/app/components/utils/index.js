"use strict";

angular.module("Utils")

.service('NotifyService', require('./notify-service'))
.service('ToastService', require('./toast-service'))

;