'use strict';

module.exports = {
	AuthenticationCtrl: ["$scope", "$rootScope", "$location", "$routeParams","AuthenticationService", 
	function($scope, $rootScope, $location, $routeParams,AuthenticationService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.navigate = function(path) {
			$location.path(path);
		};
		
		$scope.login = function(credentials) {
			AuthenticationService.login({name:credentials.username,password:credentials.password});
		};
	}],

	AccountNewCtrl: ["$scope", "$rootScope", "$location", "AuthenticationService", 
	function($scope, $rootScope, $location, AuthenticationService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/auth/md-account-form.html');

		$scope.account = {};

		$scope.Creation = true;

		$scope.signup = function() {
			AuthenticationService.signup($scope.account).then(function(){
				AuthenticationService.login({name:$scope.account.user_login,password:$scope.account.user_password});
			});
		};
	}],

	AccountResetPasswordCtrl: ["$scope", "$rootScope", "$location", "AuthenticationService",
	function($scope, $rootScope, $location, AuthenticationService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.email = "";
		$scope.result = "";

		$scope.resetPassword = function() {
			AuthenticationService.resetPassword($scope.email).then(function(response){
			});
		};
	}],

	AccountChangePasswordCtrl: ["$scope", "$rootScope", "$routeParams", "$location", "AuthenticationService", 
	function($scope, $rootScope, $routeParams, $location, AuthenticationService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.recoveryCredentials = {key:$routeParams.key};

		$scope.changePassword = function() {
			AuthenticationService.changePassword($scope.recoveryCredentials).then(function(response){
			});
		};
	}],

	AccountEditCtrl: ["$scope", "$rootScope", "$location", "$routeParams","ToastService","AuthenticationService", 
	function($scope, $rootScope, $location, $routeParams,ToastService,AuthenticationService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/auth/md-account-form.html');

		var account_id = Number($routeParams.account_id);

		AuthenticationService.getAccountById(account_id).then(function(response){
			$scope.account = response.data;
		});

		AuthenticationService.getRoles().then(function(response){
			$scope.roles = response.data;
		});

		$scope.Edit = true;

		AuthenticationService.hasRole("ADMIN").then(function(result){
			$scope.IsAdmin = result;
		});

		$scope.update = function() {

			AuthenticationService.update($scope.account).then(function(){
				//ToastService.show("Пользователь успешно сохранен");
			});
		};
	}]
};