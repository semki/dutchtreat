'use strict';

module.exports = {
	userValidation: ['$q','AuthenticationService', function($q,AuthenticationService){
		return {
			require: 'ngModel',
			restrict : 'A',
			scope:true,
			link: function($scope, iElm, iAttrs, controller) {
				if ($scope.$eval(iAttrs.userValidation))
				{
					controller.$asyncValidators.userValidation = function (modelValue, viewValue) {
				        return $q(function (resolve, reject) {
			          	    AuthenticationService.checkLogin(viewValue).then(function (response) {

			          	    	if (response.data)
			          	    	{
			          	    		resolve();
			          	    	}
			          	    	else
			          	    	{
			          	    		reject();	
			          	    	}
			            	}, function () {
		             	 		resolve();
			            	});
				        });
				    };
				}
			}
		};
	}],

	emailValidation: ['$q','AuthenticationService', function($q,AuthenticationService){
		return {
			require: 'ngModel',
			restrict : 'A',
			scope:true,
			link: function($scope, iElm, iAttrs, controller) {
				if ($scope.$eval(iAttrs.emailValidation))
				{
					controller.$asyncValidators.emailValidation = function (modelValue, viewValue) {
				        return $q(function (resolve, reject) {
				        	if (typeof(viewValue) === "undefined" || viewValue === "" || viewValue === null) {
				        		reject();
				        		return;
				        	}
			          	    AuthenticationService.checkEmail(viewValue).then(function (response) {

			          	    	if (response.data)
			          	    	{
			          	    		resolve();
			          	    	}
			          	    	else
			          	    	{
			          	    		reject();	
			          	    	}
			            	}, function () {
		             	 		reject();
			            	});
				        });
				   };
				}
			}
		};
	}],

	pwCheck: [function () {
		return {
			require: 'ngModel',
			link: function (scope, elem, attrs, ctrl) {
				var firstPassword = '#' + attrs.pwCheck;
				elem.bind('keyup', function () {
					scope.$apply(function () {
						//var v = elem.val()===$(firstPassword).val();
						var pass1 = elem.val();
						var pass2 = attrs.pwCheck;
						var result = (pass1==pass2);
						ctrl.$setValidity('pwmatch', result);
					});
				});
			}
		};
	}]
};