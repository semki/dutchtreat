"use strict";

var authControllers = require('./auth-controllers');
var authDirectives = require('./auth-directives');
var authServices = require('./auth-services');

angular.module("Authentication")

.controller('AuthenticationCtrl', authControllers.AuthenticationCtrl)
.controller('AccountNewCtrl', authControllers.AccountNewCtrl)
.controller('AccountResetPasswordCtrl', authControllers.AccountResetPasswordCtrl)
.controller('AccountChangePasswordCtrl', authControllers.AccountChangePasswordCtrl)
.controller('AccountEditCtrl', authControllers.AccountEditCtrl)

.constant('AuthConst', authServices.AuthConst)

.service('AuthenticationService', authServices.AuthenticationService)

.directive('userValidation', authDirectives.userValidation)
.directive('emailValidation', authDirectives.emailValidation)
.directive('pwCheck', authDirectives.pwCheck)

;
