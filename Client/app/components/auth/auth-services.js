'use strict';

module.exports = {
	AuthConst: {
		LOCATION_LOGIN : "/auth/login",
		LOCATION_REGISTRATION : "/auth/registration",
		LOCATION_MAIN : "/events"
	},

	AuthenticationService: ["$http","$rootScope","$location","$q","ToastService","AuthConst", "NotifyService",
	function ($http,$rootScope,$location,$q,ToastService,AuthConst, NotifyService) {
		return {
			login : function (credentials) {
				return $http({
				    method: 'POST',
				    url: "/dutch-treat/app/api/auth/login",
				    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				    transformRequest: function(obj) {
				        var str = [];
				        for(var p in obj)
				        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				        return str.join("&");
				    },
				    data: {username: credentials.name, password: credentials.password}
				})
		        .then(function(credentials) {

		        	$rootScope.isAuthenticated = true;

		        	$rootScope.currentUser = null;

		        	$rootScope.$broadcast('Authentication:updated',null);

		        	NotifyService.notify('need-refresh-events');

		            if ((typeof($rootScope.previousPage) !== "undefined" && $rootScope.previousPage !== null)) {
		            	var redirectPath = AuthConst.LOCATION_MAIN;

		            	if(($rootScope.previousPage != AuthConst.LOCATION_LOGIN) &&
		            		($rootScope.previousPage != AuthConst.LOCATION_REGISTRATION))
		            	{
		            		redirectPath = $rootScope.previousPage;
		            	}


               			$location.path(redirectPath).replace();
               			$rootScope.previousPage = null;
              		}
              		else
              		{
              			$location.path(AuthConst.LOCATION_MAIN).replace();
              		}
		        }, function(credentials) {
		            //alert("error logging in");
		            ToastService.warning("Ошибка входа. Проверьте пароль и попробуйте еще раз");

		            NotifyService.notify('need-refresh-events');
		        });
			},

			logout : function () {
				return $http({
				    method: 'POST',
				    url: "/dutch-treat/app/api/auth/logout",
				    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				})
		        .then(function(credentials) {
		        	$rootScope.currentUser = null;
					$rootScope.isAuthenticated = null;
					$rootScope.$broadcast('Authentication:updated',null);

					$location.path(AuthConst.LOCATION_LOGIN).replace();
					NotifyService.notify('need-refresh-events');

		        }, function(credentials) {
		        	$rootScope.currentUser = null;
					$rootScope.isAuthenticated = null;
					NotifyService.notify('need-refresh-events');
		            //alert("error logout");
		        });
			},

			signup : function(account){
				return $http({
				    method: 'POST',
				    url: "/dutch-treat/app/api/auth/registration",
				    data: account
				})
		        .then(function(response) {
		        	ToastService.show("пользователь успешно зарегистрирован");
		        }, function(response) {
		        	if (response.status == 400)
		        	{
		        		ToastService.warning(response.data.Message);
		        	}
		            return $q.reject();
		        });
			},

			resetPassword : function(email){
				return $http({
				    method: 'GET',
				    url: "/dutch-treat/app/api/auth/resetpassword",
				    params: {email:email}
				})
		        .then(function(response) {
		        	ToastService.show("Email с инструкциями успешно отправлен.");
		        }, function(response) {
		        	if (response.status == 400)
		        	{
		        		ToastService.warning(response.data.Message);
		        	}
		            return $q.reject();
		        });
			},

			changePassword : function(changeCredentials) {
				return $http({
					    method: 'POST',
					    url: "/dutch-treat/app/api/auth/changepassword",
					    data: changeCredentials
					})
			        .then(function(response) {
			        	ToastService.show("Пароль успешно изменен.");
			        }, function(response) {
			        	if (response.status == 400)
			        	{
			        		ToastService.warning(response.data.Message);
			        	}
			            return $q.reject();
			        });
			},

			update : function(account){
				return $http({
				    method: 'POST',
				    url: "/dutch-treat/app/api/auth/account/edit",
				    data: account
				}).then(function(response) {
		        		ToastService.show("пользователь успешно сохранен");
			        }, function(response) {
			        	if (response.status == 400)
			        	{
			        		ToastService.warning(response.data.Message);
			        	}
			            return $q.reject();
			        });
			},

			getCurrentUser : function(){
				return	$http({
					    method: 'GET',
					    url: "/dutch-treat/app/api/auth/current"
					})
			        .then(function(response) {
			        	$rootScope.currentUser = response.data;
			        	$rootScope.authenticated = true;
						return {currentUser:$rootScope.currentUser,isAuthenticated:$rootScope.authenticated};
			        }, function() {
			            console.log("current user service error");
			        });
			},

			getAccountById : function(accId){
				return $http({
						    method: 'GET',
						    url: "/dutch-treat/app/api/auth/account/"+accId
						});
			},

			getAccountList : function(){
				return $http({
						    method: 'GET',
						    url: "/dutch-treat/app/api/auth/account"
						});
			},

			getRoles : function(){
				return $http({
						    method: 'GET',
						    url: "/dutch-treat/app/api/auth/roles"
						});
			},

			checkLogin : function(login){
				return $http({
							    method: 'GET',
							    url: "/dutch-treat/app/api/auth/checkuser",
							    params: {username:login}
							});
			},

			checkEmail : function(email){
				return $http({
							    method: 'GET',
							    url: "/dutch-treat/app/api/auth/checkemail",
							    params: {email:email}
							});
			},

			isAuthenticated : function(){
				var currentService = this;
				return $q(function(resolve,reject){
					if(typeof($rootScope.currentUser) !== "undefined" && $rootScope.currentUser !== null)
					{
						resolve({currentUser:$rootScope.currentUser,isAuthenticated:$rootScope.authenticated});
					}
					else
					{
						currentService.getCurrentUser().then(function(){
							resolve({currentUser:$rootScope.currentUser,isAuthenticated:$rootScope.authenticated});
						},function(){
							reject();
						});
					}
				});
			},

			hasRole : function(roleName){
				var currentService = this;
				return $q(function(resolve,reject){
					currentService.isAuthenticated().then(function(response){
						var user = response.currentUser;

						if (user && user.roles)
						{
							user.roles.forEach(function(item, i, arr) {

								if (item.name==roleName)
								{
									resolve(true);
								}
							});
						}

						resolve(false);
					},function(){
						resolve(false);
					});

				});
			}
		};
	}]
};
