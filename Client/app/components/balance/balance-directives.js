"use strict";

module.exports = {
  summaryBalance: ["BalanceService", 
  function(BalanceService) {
    return {
    	restrict: 'E',
    	scope: {
        eventId: '@'
      },
      link: function(scope) {
      	scope.$watch('eventId', function(newValue, oldValue) {
      		if (newValue !== null && newValue !== "") {
      			BalanceService.getSummary(newValue).then(function(responseDTO) {
      				scope.balanceRows = responseDTO.data.balanceRows;
      			});
      		}
      	});
  	},
      templateUrl: require('views/balance/md-balance-summary-table.html')
    };
  }]
};