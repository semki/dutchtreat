'use strict';

module.exports = {

	ParticipantBalanceCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "BalanceService",
	function($scope, $rootScope, $location, $routeParams, EventosService, BalanceService) {
		$rootScope.$path = $location.path.bind($location);

		var event_id = Number($routeParams.event_id);
		var participant_id = Number($routeParams.participant_id);

		$scope.evento = EventosService.getEvento(event_id);
		if (participant_id > 0){
			BalanceService.getCalculationByPariticipantId(participant_id).then(function(response_balance) {
					$scope.calculation = response_balance.data;
					$scope.target_participant = response_balance.data.participant;
				});
		}


		$scope.changeTargetParticipant = function() {
			BalanceService.getCalculation($scope.target_participant).then(function(response_balance) {
				$scope.calculation = response_balance.data;
			});
		};

		$scope.$watch('target_participant', function() {
			if (typeof($scope.target_participant) !== "undefined" && $scope.target_participant !== null) {
				BalanceService.getCalculation($scope.target_participant).then(function(response_balance) {
					$scope.calculation = response_balance.data;
				});
			}
		});
	}]
};