'use strict';

module.exports = {
	BalanceService: ["$http",
	function ($http) {
		return {
			getCalculation : function (pariticipant) {
				return $http.get("/dutch-treat/app/api/balance/by-participant/" + pariticipant.id);
			},
			getSummary : function (eventId) {
				return $http.get("/dutch-treat/app/api/balance/summary-by-event/" + eventId);
			},
			getCalculationByPariticipantId : function (pariticipantId) {
				return $http.get("/dutch-treat/app/api/balance/by-participant/" + pariticipantId);
			}
		};
	}]
};