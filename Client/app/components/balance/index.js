"use strict";

var balanceControllers = require('./balance-controllers');
var balanceDirectives = require('./balance-directives');
var balanceServices = require('./balance-services');

angular.module("Balance")

.controller('ParticipantBalanceCtrl', balanceControllers.ParticipantBalanceCtrl)

.service('BalanceService', balanceServices.BalanceService)

.directive('summaryBalance', balanceDirectives.summaryBalance)

;
