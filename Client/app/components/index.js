'use strict';
angular.module("semki.DutchTreat")

.controller("MainCtrl", require('./main-controller'))
.controller("MenuCtrl", require('./menu-controller'))
;

require('./admin');
require('./auth');
require('./balance');
require('./event');
require('./participants');
require('./purchases');
require('./transfers');
require('./utils');
