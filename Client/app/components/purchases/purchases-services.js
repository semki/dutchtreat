"use strict";

module.exports = {

	PurchasesRest: ["$resource",
	function($resource) {
		return $resource("/dutch-treat/app/api/purchases/:id", {id: "@id"});
	}],

	PurchaseService: ["PurchasesRest",
	function (PurchasesRest) {
		return {
			getPurchases : function (event_id) {
				return PurchasesRest.query({event_id: event_id});
			},
			addPurchase : function (purchase) {
				return purchase.$save();
			},
			updatePurchase : function ( purchase ) {
				purchase.date.setUTCDate(purchase.date.getDate());
				purchase.date.setHours(0);
				return purchase.$save();
			},
			createPurchase: function(evento) {
				var purchase = new PurchasesRest();
				purchase.event_id = evento.id;
				purchase.consumers = evento.participants;
				purchase.date = new Date();
				return purchase;
			},
			getPurchase : function ( id ) {
				var updatedPurchase = PurchasesRest.get({id: id});
				//console.log("date is"+updatedPurchase.date);
				return updatedPurchase;
			},
			deletePurchase : function ( purchase ) {
				return purchase.$delete();
			}
		};
	}]
};