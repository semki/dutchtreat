"use strict";

module.exports = {
	purchaseTable: ["PurchaseService", "$mdDialog", "$location",
	function(PurchaseService, $mdDialog, $location) {
	return {
		restrict: 'E',
		scope: {
			eventId: '@'
		},
		link: function(scope) {
			scope.$watch('eventId', function(newValue, oldValue) {
				if (newValue !== "") {
					scope.purchases = PurchaseService.getPurchases(newValue);
				}
			});

			scope.navigateToEditPurchase = function(purchase) {
				$location.path("events/"+scope.eventId+"/purchases/"+purchase.id+"/edit");
			};

			scope.confirmDelete = function(ev, purchase) {
				var confirm = $mdDialog.confirm()
				   		.title("Подтверждение")
						.textContent("Вы уверены, что хотите удалить покупку " + purchase.description + "?")
						.ariaLabel('Confirm delete')
						.targetEvent(ev)
						.clickOutsideToClose(true)
						.ok('OK')
						.cancel('Отмена');
				$mdDialog.show(confirm).then(function () {
				  	PurchaseService.deletePurchase(purchase).then(function() {
						scope.purchases = PurchaseService.getPurchases(scope.eventId);
					});
				});
			};
		},
		
		templateUrl: require('views/purchases/md-purchases-list.html')
		};
	}]
};