"use strict";

module.exports = {

	PurchaseNewCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "$q", "EventosService", "PurchaseService",
	function($scope, $rootScope, $location, $routeParams, $q, EventosService, PurchaseService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/purchases/md-purchase-form.html');

		var event_id = Number($routeParams.event_id);

		var currentUser = $rootScope.currentUser;
		var _ = require('lodash');
		$scope.evento = EventosService.getEvento(event_id);
		$scope.evento.$promise.then(function() {
			$scope.purchase = PurchaseService.createPurchase($scope.evento);
			var currentUserParitcipant = _.find($scope.evento.participants, function(participant) {
				return participant.linkedAccount ? (participant.linkedAccount.id == currentUser.id) : false;
			});
			if (currentUserParitcipant !== undefined) {
				$scope.purchase.buyer = currentUserParitcipant;
			}
		});

		$scope.navigate = function(path) {
			$location.path(path);
		};

		$scope.addPurchase = function() {
			PurchaseService.addPurchase($scope.purchase).then(function() {
				$location.path('events/' + event_id);
			});
		};
	}],

	PurchaseCreateCopyCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "PurchaseService",
	function($scope, $rootScope, $location, $routeParams, EventosService, PurchaseService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/purchases/md-purchase-form.html');

		var event_id = Number($routeParams.event_id);

		$scope.purchase = PurchaseService.getPurchase($routeParams.id);
		$scope.purchase.$promise.then(function() {
				$scope.purchase.date = new Date();
				$scope.purchase.date.setUTCDate($scope.purchase.date.getDate());
				$scope.purchase.date.setHours(0);
				$scope.purchase.id = "";
			});
		$scope.evento = EventosService.getEvento(event_id);

		$scope.navigate = function(path) {
			$location.path(path);
		};

		$scope.addPurchase = function() {
			PurchaseService.addPurchase($scope.purchase).then(function() {
				$location.path('events/' + event_id);
			});
		};
	}],

	PurchaseEditCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "PurchaseService",
	function($scope, $rootScope, $location, $routeParams, EventosService, PurchaseService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/purchases/md-purchase-form.html');

		var event_id = Number($routeParams.event_id);

		$scope.purchase = PurchaseService.getPurchase($routeParams.id);

		$scope.evento = EventosService.getEvento(event_id);

		$scope.navigate = function(path) {
			$location.path(path);
		};

		$scope.editPurchase = function() {
			PurchaseService.updatePurchase($scope.purchase).then(function() {
				$location.path('events/' + event_id);
			});
		};
	}]
};
