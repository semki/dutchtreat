'use strict';

module.exports = ["$scope", "$location", "$mdSidenav", "EventosService", 
	function($scope, $location, $mdSidenav, EventosService) {

		$scope.eventos = EventosService.getEventos();

		$scope.navigateToEvento = function (evento) {
			$location.path('events/' + evento.id);
		};

		$scope.close = function () {
	      $mdSidenav('main-menu').close();
	    };
}];