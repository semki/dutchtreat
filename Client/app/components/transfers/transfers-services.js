"use strict";

module.exports = {
	TransfersRest: ["$resource",
	function($resource) {
		return $resource("/dutch-treat/app/api/transfers/:id", {id: "@id"});
	}],

	TransferService: ["TransfersRest",
	function (TransfersRest) {
		return {
			getTransfers : function (event_id) {
				return TransfersRest.query({event_id: event_id});
			},
			addTransfer : function (transfer) {
				return transfer.$save();
			},
			updateTransfer : function ( transfer ) {
				transfer.transferdate.setUTCDate(transfer.transferdate.getDate());
				transfer.transferdate.setHours(0);
				return transfer.$save();
			},
			createTransfer : function (event_id) {
				var transfer = new TransfersRest();
				transfer.event_id = event_id;
				transfer.transferdate = new Date();
				return transfer;
			},
			getTransfer : function ( id ) {
				return TransfersRest.get({id : id});
			},

			deleteTransfer : function ( transfer ) {
				return transfer.$delete();
			}
		};
	}]
};