"use strict";

/*
	TODO find a good place for this, or make a better solution
*/
var regexIso8601 = /^\d{4}-\d{2}-\d{2}/;

function convertDateStringsToDates(input) {
    // Ignore things that aren't objects.
    if (typeof input !== "object") return input;

    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;

        var value = input[key];
        var match;
        // Check for string properties which look like dates.
        if (typeof value === "string" && (match = value.match(regexIso8601))) {
            var milliseconds = Date.parse(match[0]);
            if (!isNaN(milliseconds)) {
                input[key] = new Date(milliseconds);
            }
        } else if (typeof value === "object") {
            // Recurse into object
            convertDateStringsToDates(value);
        }
    }
}

var transfersControllers = require('./transfers-controllers');
var transfersDirectives = require('./transfers-directives');
var transfersServices = require('./transfers-services');

angular.module("Transfers")

.controller('TransfersCtrl', transfersControllers.TransfersCtrl)
.controller('TransferNewCtrl', transfersControllers.TransferNewCtrl)
.controller('TransferCreateCopyCtrl', transfersControllers.TransferCreateCopyCtrl)
.controller('TransferEditCtrl', transfersControllers.TransferEditCtrl)
.directive('transfersTable', transfersDirectives.transfersTable)
.factory('TransfersRest', transfersServices.TransfersRest)
.service('TransferService', transfersServices.TransferService)


.config(["$httpProvider", function ($httpProvider) {
     $httpProvider.defaults.transformResponse.push(function(responseData){
        convertDateStringsToDates(responseData);
        return responseData;
    });
}])
;