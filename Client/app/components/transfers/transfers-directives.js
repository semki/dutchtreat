"use strict";

module.exports = {
	transfersTable: ["TransferService", "$location", "$mdDialog",
	function(TransferService, $location, $mdDialog) {
		return {
		  	restrict: 'E',
		  	scope: {
		      eventId: '@'
		    },
		    link: function(scope) {
		    	scope.$watch('eventId', function(newValue, oldValue) {
		    		if (newValue !== null && newValue !== "") {
		    			scope.transfers = TransferService.getTransfers(newValue);
		    			scope.promise = scope.transfers.$promise;
		    		}
		    	});

				scope.selected = [];

				scope.query = {
					order: 'name',
					limit: 5,
					page: 1
				};

				scope.onPaginate = function (page, limit) {
					//getDesserts(angular.extend({}, $scope.query, {page: page, limit: limit}));
				};

				scope.onReorder = function (order) {
					//getDesserts(angular.extend({}, $scope.query, {order: order}));
				};

		    	scope.navigateToEditTransfer = function(transfer) {
					$location.path("events/"+scope.eventId+"/transfers/"+transfer.id+"/edit");
				};

				scope.confirmDelete = function(ev, transfer) {
					var confirm = $mdDialog.confirm()
					   		.title("Подтверждение")
							.textContent("Вы уверены, что хотите удалить проводку " + transfer.description + "?")
							.ariaLabel('Confirm delete')
							.targetEvent(ev)
							.clickOutsideToClose(true)
							.ok('OK')
							.cancel('Отмена');
					$mdDialog.show(confirm).then(function () {
					  	TransferService.deleteTransfer(transfer).then(function() {
							scope.transfers = TransferService.getTransfers(scope.eventId);
						});
					});
				};
			},
		    templateUrl: require('views/transfers/md-transfers-list.html')
		  
		};
	}]
};