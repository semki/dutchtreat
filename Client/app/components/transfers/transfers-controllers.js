"use strict";

module.exports = {
	TransfersCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "TransferService",
	function($scope, $rootScope, $location, $routeParams, EventosService, TransferService) {
		$rootScope.$path = $location.path.bind($location);
		var event_id = Number($routeParams.event_id);
		$scope.evento = EventosService.getEvento(event_id);
		$scope.transfers = TransferService.getTransfers($routeParams.event_id);


		$scope.deleteTransfer = function(transfer) {
			TransferService.deleteTransfer(transfer).then(function() {
				$scope.transfers = TransferService.getTransfers($routeParams.event_id);
				$scope.promise = $scope.transfers.$promise;
			});
		};

		$scope.navigateToEditTransfer = function(transfer) {
			$location.path("events/"+event_id+"/transfers/"+transfer.id+"/edit");
		};
	}],

	TransferNewCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "TransferService",
	function($scope, $rootScope, $location, $routeParams, EventosService, TransferService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/transfers/md-transfer-form.html');

		var event_id = Number($routeParams.event_id);

		$scope.evento = EventosService.getEvento(event_id);

		$scope.navigate = function(path) {
			$location.path(path);
		};

		var currentUser = $rootScope.currentUser;
		var _ = require('lodash');

		$scope.transfer = TransferService.createTransfer(event_id);
		$scope.evento.$promise.then(function() {
			var currentUserParitcipant = _.find($scope.evento.participants, function(participant) {
				return participant.linkedAccount ? (participant.linkedAccount.id == currentUser.id) : false;
			});
			if (currentUserParitcipant !== undefined) {
				$scope.transfer.sender = currentUserParitcipant;
			}
		});
		$scope.addTransfer = function() {
			TransferService.addTransfer($scope.transfer).then(function() {
				$location.path("events/" + event_id);
			});
		};
	}],

	TransferCreateCopyCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "TransferService",
	function($scope, $rootScope, $location, $routeParams, EventosService, TransferService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/transfers/md-transfer-form.html');

		var event_id = Number($routeParams.event_id);
		var resetTransfer = function() {
			$scope.transfer.transferdate = new Date();
			$scope.transfer.transferdate.setUTCDate($scope.transfer.transferdate.getDate());
			$scope.transfer.transferdate.setHours(0);
			$scope.transfer.id = "";
		};

		$scope.evento = EventosService.getEvento(event_id);
		$scope.evento.$promise.then(function() {
			$scope.transfer = TransferService.getTransfer($routeParams.id);
			$scope.transfer.$promise.then(resetTransfer);
		});

		$scope.navigate = function(path) {
			$location.path(path);
		};

		$scope.addTransfer = function() {
			TransferService.addTransfer($scope.transfer).then(function() {
				$location.path("events/" + event_id);
			});
		};
	}],

	TransferEditCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "TransferService",
	function($scope, $rootScope, $location, $routeParams, EventosService, TransferService) {
		$rootScope.$path = $location.path.bind($location);

		$scope.formUrl = require('views/transfers/md-transfer-form.html');

		var event_id = Number($routeParams.event_id);

		$scope.transfer = TransferService.getTransfer($routeParams.id);

		$scope.navigate = function(path) {
			$location.path(path);
		};

		$scope.evento = EventosService.getEvento(event_id);

		$scope.editTransfer = function() {
			TransferService.updateTransfer($scope.transfer).then(function() {
				$location.path("events/" + event_id);
			});
		};
	}]
};
