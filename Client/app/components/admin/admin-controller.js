'use strict';

module.exports = {
	AdminUserListCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "AuthenticationService", 
		function($scope, $rootScope, $location, $routeParams,AuthenticationService) {
			$rootScope.$path = $location.path.bind($location);

			AuthenticationService.getAccountList().then(function(accounts){
				$scope.accounts = accounts.data;
			});
		}]
};