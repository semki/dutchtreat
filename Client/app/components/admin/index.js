"use strict";

var adminControllers = require('./admin-controller');

angular.module("Admin")

.controller('AdminUserListCtrl', adminControllers.AdminUserListCtrl)
;
