'use strict';
module.exports = ["$scope", "$rootScope", "$location", "$routeParams", "$mdSidenav", "AuthenticationService",
	function($scope, $rootScope, $location, $routeParams, $mdSidenav, AuthenticationService) {

		var CHANGE_PASSWORD_URL = "/auth/changepassword";

		var ExceptionAuthUrlList = [CHANGE_PASSWORD_URL];

		function urlInExceptionList(url)
		{
			if (ExceptionAuthUrlList.indexOf(url)>-1)
			{
				return true;
			}
			else
			{
				return false;
			}
		} 

		function checkAuth()
		{
			AuthenticationService.isAuthenticated().then(function(authData){
				$scope.isAuthenticated = authData.isAuthenticated;
				$scope.currentUser = authData.currentUser;
			});
		}

		function checkRole()
		{
			AuthenticationService.hasRole("ADMIN").then(function(result){
				$scope.IsAdmin = result;
			});
		}

		if (!urlInExceptionList($location.$$path))
		{
			checkAuth();
			checkRole();
		}

		$scope.$on('Authentication:updated', function(event,data) {
			checkAuth();
			checkRole();
		});

		$scope.login = function() {
			
		};

		$scope.logout = function() {
			AuthenticationService.logout();
		};

		$scope.toggleMenu = function() {
			$mdSidenav('main-menu').toggle();
		};

		$scope.navigateToEventosList = function() {
			$location.path('events/');
		};
}];