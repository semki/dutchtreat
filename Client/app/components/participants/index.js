"use strict";

var paricipantServices = require('./participants-service');

angular.module('Participants')

.factory('ParticipantsRest', paricipantServices.ParticipantsRest)
.service('ParticipantService', paricipantServices.ParticipantService)

;