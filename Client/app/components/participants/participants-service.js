'use strict';

module.exports = {
	ParticipantsRest: ["$resource", 
	function($resource) {
		return $resource("/dutch-treat/app/api/participants/:id", {id: "@id"});
	}],

	ParticipantService: ["ParticipantsRest", 
	function (ParticipantsRest) {
		return {
			getParticipants : function (event_id) {
				return ParticipantsRest.get({id:event_id});
			}			
		};
	}]
};