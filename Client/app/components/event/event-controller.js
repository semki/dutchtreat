'use strict';

module.exports = {
	EventosCtrl: ["$scope", "$rootScope", "$location", "$mdDialog", "EventosService","AuthenticationService", "NotifyService",
	function($scope, $rootScope, $location, $mdDialog, EventosService,AuthenticationService, NotifyService) {
		$rootScope.$path = $location.path.bind($location);
		$scope.eventos = EventosService.getEventos();

		AuthenticationService.hasRole("ADMIN").then(function(result){
			$scope.canDelete = result;
		});

		NotifyService.subscribe($scope, 'need-refresh-events', function(data) {
			$scope.eventos = EventosService.getEventos();
		});

		$scope.navigateToEvento = function (evento) {
			$location.path('events/' + evento.id);
		};

		$scope.navigateToEditEvento = function (evento) {
			$location.path('events/' + evento.id + "/edit");
		};

		$scope.confirmDelete = function(ev, evento) {
			var confirm = $mdDialog.confirm()
					.title("Подтверждение")
					.textContent("Вы уверены, что хотите удалить событие " + evento.name + "?")
					.ariaLabel('Confirm delete')
					.targetEvent(ev)
					.clickOutsideToClose(true)
					.ok('OK')
					.cancel('Отмена');
			$mdDialog.show(confirm).then(function () {
			  	EventosService.deleteEvento(evento).then(function() {
					NotifyService.notify('need-refresh-events');
				});
			});
		};
	}],

	EventoNewCtrl: ["$scope", "$rootScope", "$location","$http", "$mdConstant", "EventosService","AuthenticationService", "NotifyService",
	function($scope, $rootScope, $location,$http, $mdConstant, EventosService,AuthenticationService, NotifyService) {
		$rootScope.$path = $location.path.bind($location);
		$scope.evento = EventosService.createEvento();
		$scope.evento.participants = [];
		$scope.evento.accessAccounts = [];

		$scope.formUrl = require('views/events/md-event-form.html');

		$scope.separatorKeys = [$mdConstant.KEY_CODE.ENTER,
														$mdConstant.KEY_CODE.COMMA,
														$mdConstant.KEY_CODE.SEMICOLON,
														$mdConstant.KEY_CODE.TAB];
		/*KEY_CODE: {
      COMMA: 188,
      SEMICOLON : 186,
      ENTER: 13,
      ESCAPE: 27,
      SPACE: 32,
      PAGE_UP: 33,
      PAGE_DOWN: 34,
      END: 35,
      HOME: 36,
      LEFT_ARROW : 37,
      UP_ARROW : 38,
      RIGHT_ARROW : 39,
      DOWN_ARROW : 40,
      TAB : 9,
      BACKSPACE: 8,
      DELETE: 46
			*/

		var _ = require('lodash');
		AuthenticationService.getAccountList().then(function(response){
			$scope.accounts = response.data;

			if (typeof($rootScope.currentUser) !== "undefined" && $rootScope.currentUser !== null) {
				_.forEach($scope.accounts, function(account) {
					if ($rootScope.currentUser.id == account.id) {
						$scope.evento.accessAccounts.push(account);
					}
				});
			}
		});

		$scope.multiselectLocalization = {
			selectAll       : "Все",
		    selectNone      : "Очистить",
		    reset           : "Сбросить",
		    search          : "Найти...",
		    nothingSelected : "Пусто"
		};

		$scope.addEvento = function() {
			EventosService.addEvento($scope.evento).then(function() {
				$location.path('events');
				NotifyService.notify('need-refresh-events');
			});
		};

		$scope.newParticipant = function (participantName) {
			return {
				name: participantName
			};
		};
	}],

	EventoEditCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "$mdConstant", "EventosService","AuthenticationService", "NotifyService",
	function($scope, $rootScope, $location, $routeParams, $mdConstant, EventosService,AuthenticationService, NotifyService) {
		$rootScope.$path = $location.path.bind($location);
		$scope.evento = EventosService.getEvento($routeParams.id);
		$scope.formUrl = require('views/events/md-event-form.html');

		if (typeof($scope.evento.participants) === "undefined" || $scope.evento.participants === null) {
			$scope.evento.participants = [];
		}

		$scope.$watch('evento.invate', function(newValue, oldValue) {
			var currentBasePath = $location.absUrl().split("edit")[0];

			$scope.inviteHash = currentBasePath+"addUser/"+$scope.evento.invate;
		});

		$scope.separatorKeys = [$mdConstant.KEY_CODE.ENTER,
														$mdConstant.KEY_CODE.COMMA,
														$mdConstant.KEY_CODE.SEMICOLON,
														$mdConstant.KEY_CODE.TAB];

		AuthenticationService.getAccountList().then(function(response){
			$scope.accounts = response.data;

			$scope.evento.$promise.then(function(value) {
				var accessAccountsIds = $scope.evento.accessAccounts.map(function(account) {
					return account.id;
				});

				$scope.accounts.map(function(account) {
					account.selected = (accessAccountsIds.indexOf(account.id) >=0 );
				});
			});
		});

		$scope.multiselectLocalization = {
			selectAll       : "Все",
		    selectNone      : "Очистить",
		    reset           : "Сбросить",
		    search          : "Найти...",
		    nothingSelected : "Пусто"
		};

		$scope.navigateTo = function (url) {
			$location.path(url);
		};

		$scope.editEvento = function() {
			EventosService.updateEvento($scope.evento).then(function() {
				$location.path('events');
				NotifyService.notify('need-refresh-events');
			});
		};

		$scope.invateUser = function() {
			EventosService.invateUser($scope.evento).then(function(evento) {
				$scope.evento = evento;
			});
		};

		$scope.newParticipant = function (participantName) {
			return {
				name: participantName,
				event_id: $routeParams.id
			};
		};
	}],

	EventoUrlEditCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "ToastService", "EventosService","AuthenticationService", "NotifyService",
	function($scope, $rootScope, $location, $routeParams, ToastService, EventosService, AuthenticationService, NotifyService) {
		var invite = $routeParams.invateHash;

		EventosService.addUser($routeParams.id,$routeParams.invateHash).then(function(evento){
			ToastService.show("пользователь успешно добавлен к событию");
			$scope.evento = evento;
			NotifyService.notify('need-refresh-events');
		},function(){
			ToastService.warning("ошибка при добавлении поьзователя");
			$location.path("events").replace();
		});
	}],

	EventoViewCtrl: ["$scope", "$rootScope", "$location", "$routeParams", "EventosService", "PurchaseService",
	function($scope, $rootScope, $location, $routeParams, EventosService, PurchaseService) {
		$rootScope.$path = $location.path.bind($location);
		$scope.evento = EventosService.getEvento($routeParams.id);
	}],

	EventoParticipantsLinkCtrl: ["$rootScope", "$scope", "$location", "$routeParams", "EventosService",
	function ($rootScope, $scope, $location, $routeParams, EventosService) {

		var _ = require('lodash');

		$scope.updateAvailableAccounts = function () {
			var alreadyLinkedAccountIds = _.filter(_.map($scope.evento.participants, function (participant, idx, col) {
				return participant.linkedAccount ? participant.linkedAccount.id : -1;
			}), function(id) {
				return id >= 0;
			});

			$scope.availableAccounts = _.filter($scope.evento.accessAccounts, function(account) {
				return !_.some(alreadyLinkedAccountIds, function (testAccountId) {
					return testAccountId === account.id;
				});
			});

		};

		$rootScope.$path = $location.path.bind($location);
		$scope.availableAccounts = [];
		$scope.evento = EventosService.getEvento($routeParams.id);
		$scope.evento.$promise.then($scope.updateAvailableAccounts);

		$scope.getAviableAccounts = function (participant) {
			var newArray = _.concat($scope.availableAccounts, participant.linkedAccount ? participant.linkedAccount : []);
			return _.uniqBy(newArray, 'id');
		};

		$scope.removeLink = function(participant) {
			participant.linkedAccount = null;
			$scope.updateAvailableAccounts();
		};

		$scope.navigateTo = function (url) {
			$location.path(url);
		};

		$scope.editEvento = function() {
			EventosService.updateEvento($scope.evento).then(function() {
				$location.path('events');
			});
		};
	}]
};
