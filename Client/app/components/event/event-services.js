"use strict";

module.exports = {
	EventosRest: ["$resource", 
	function($resource) {
		return $resource("/dutch-treat/app/api/eventos/:id", {id: "@id"});
	}],

	EventosService: ["$http", "EventosRest",
	function ($http, EventosRest) {
		return {
			getEventos : function () {
				return EventosRest.query();
			},
			addEvento : function (evento) {
				return evento.$save();
			},
			updateEvento : function ( evento ) {
				return evento.$save();
			},
			invateUser : function ( evento ) {
				return $http({
						url: "/dutch-treat/app/api/eventos/" + evento.id + "/createInvate"
					});
			},
			addUser : function ( eventId,invateHash ) {
				return $http({
						url: "/dutch-treat/app/api/eventos/" + eventId + "/addUser/" + invateHash
					});
			},
			createEvento : function () {
				return new EventosRest();
			},
			getEvento : function ( id ) {
				return EventosRest.get({id : id});
			},
			deleteEvento : function ( evento ) {
				return evento.$delete();
			}
		};
	}]
};