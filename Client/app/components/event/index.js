"use strict";

var eventControllers = require('./event-controller');
var eventServices = require('./event-services');

angular.module("Eventos")

.controller('EventosCtrl', eventControllers.EventosCtrl)
.controller('EventoNewCtrl', eventControllers.EventoNewCtrl)
.controller('EventoEditCtrl', eventControllers.EventoEditCtrl)
.controller('EventoUrlEditCtrl', eventControllers.EventoUrlEditCtrl)
.controller('EventoViewCtrl', eventControllers.EventoViewCtrl)
.controller('EventoParticipantsLinkCtrl', eventControllers.EventoParticipantsLinkCtrl)

.factory('EventosRest', eventServices.EventosRest)
.service('EventosService', eventServices.EventosService)
;
