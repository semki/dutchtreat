require('config/styles');

angular.module("Utils",["ngMaterial"]);
angular.module('Participants', []);
angular.module("Purchases", ["Utils"]);
angular.module("Transfers", ["Utils"]);
angular.module("Balance", ["Eventos", "Utils"]);
angular.module("Authentication",["Utils"]);
angular.module("Admin",["Utils"]);
angular.module("Eventos", ["ngRoute", "Purchases", "ngResource","Authentication", "Utils"]);

angular.module('semki.DutchTreat', [
  "ngAnimate", "ngRoute","ngResource", "ngMessages",
  "angular-loading-bar", "ngMaterial",
  "md.data.table",
  "Eventos", "Purchases", "Transfers", "Balance","Authentication","Admin", "Utils"])  

.config(require('config/routes'))
.config(require('config/http-interceptor'))
.config(require('config/date-provider'))
.config(require('config/material-theme-provider'))
;  

require('components');
