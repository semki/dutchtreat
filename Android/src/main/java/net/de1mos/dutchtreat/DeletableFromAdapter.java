package net.de1mos.dutchtreat;

/**
 * Created by denis on 22.08.16.
 */
public interface DeletableFromAdapter {
    void deleteItem(int position);
}
