package net.de1mos.dutchtreat.adapters;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import net.de1mos.dutchtreat.DeletableFromAdapter;
import net.de1mos.dutchtreat.R;

import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 * Created by denis on 08.08.16.
 */
public class TrasferCardAdapter extends RecyclerView.Adapter<TrasferCardAdapter.TransferViewHolder> {

    private final DeletableFromAdapter deletableFromAdapter;
    private List<TransferItemContainer> mDataset;

    public TrasferCardAdapter(List<TransferItemContainer> inputDataset, DeletableFromAdapter deletableFromAdapter) {
        this.mDataset = inputDataset;
        this.deletableFromAdapter = deletableFromAdapter;
    }

    @Override
    public TransferViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transfer_item, parent, false);
        return new TransferViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TransferViewHolder holder, final int position) {
        TransferItemContainer container = mDataset.get(holder.getAdapterPosition());
        holder.senderName.setText(container.senderName);
        holder.receiverName.setText(container.receiverName);
        holder.amount.setText(String.format(Locale.getDefault(), "%.2f %s", container.amount, Currency.getInstance(Locale.getDefault()).getSymbol()));
        holder.description.setText(container.description);
        holder.date.setText(container.transferdate);
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteItem(holder.getAdapterPosition(), view);
            }
        });
    }

    private void deleteItem(final int position, View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(view.getContext().getString(R.string.confirm_title));
        builder.setMessage(view.getContext().getString(R.string.confim_sure_body));

        builder.setPositiveButton(view.getContext().getString(R.string.confirm_yes_button), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                deletableFromAdapter.deleteItem(position);
            }
        });

        builder.setNegativeButton(view.getContext().getString(R.string.confirm_no_button), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class TransferViewHolder extends RecyclerView.ViewHolder {
        public TextView senderName;
        public TextView receiverName;
        public TextView amount;
        public TextView description;
        public TextView date;
        public ImageButton deleteButton;

        public TransferViewHolder(View v) {
            super(v);
            senderName = (TextView) v.findViewById(R.id.transfer_list_item_sender);
            receiverName = (TextView) v.findViewById(R.id.transfer_list_item_receiver);
            amount = (TextView) v.findViewById(R.id.transfer_list_item_amount);
            description = (TextView) v.findViewById(R.id.transfer_list_item_description);
            date = (TextView) v.findViewById(R.id.transfer_list_item_date);
            deleteButton = (ImageButton) v.findViewById(R.id.transfer_list_delete_item_button);
        }
    }

    public static class TransferItemContainer {
        public TransferItemContainer(Integer id, Integer eventId, Float amount, String description, String senderName, String receiverName, String transferdate) {
            this.id = id;
            this.eventId = eventId;
            this.amount = amount;
            this.description = description;
            this.senderName = senderName;
            this.receiverName = receiverName;
            this.transferdate = transferdate;
        }

        public TransferItemContainer() {
        }

        public Integer id;
        public Integer eventId;
        public Float amount;
        public String description;
        public String senderName;
        public String receiverName;
        public String transferdate;
    }
}
