package net.de1mos.dutchtreat.adapters;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import net.de1mos.dutchtreat.DeletableFromAdapter;
import net.de1mos.dutchtreat.R;

import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 * Created by denis on 08.08.16.
 */
public class PurchaseCardAdapter extends RecyclerView.Adapter<PurchaseCardAdapter.PurchaseViewHolder> {

    private final DeletableFromAdapter deletableFromAdapter;
    private List<PurchaseItemContainer> dataset;

    public PurchaseCardAdapter(List<PurchaseItemContainer> inputDataset, DeletableFromAdapter deletableFromAdapter) {
        this.deletableFromAdapter = deletableFromAdapter;
        this.dataset = inputDataset;
    }

    @Override
    public PurchaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_item, parent, false);
        return new PurchaseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PurchaseViewHolder holder, final int position) {
        PurchaseItemContainer container = dataset.get(holder.getAdapterPosition());
        holder.buyerName.setText(container.buyerName);
        holder.consumersCount.setText(String.format(Locale.getDefault(), "%s", container.consumersCount));
        String textAmount = String.format(Locale.getDefault(), "%.2f %s", container.amount, Currency.getInstance(Locale.getDefault()).getSymbol());
        holder.amount.setText(textAmount);
        holder.description.setText(container.description);
        holder.date.setText(container.date);
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteItem(holder.getAdapterPosition(), view);

            }
        });
    }

    private void deleteItem(final int position, View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(view.getContext().getString(R.string.confirm_title));
        builder.setMessage(view.getContext().getString(R.string.confim_sure_body));

        builder.setPositiveButton(view.getContext().getString(R.string.confirm_yes_button), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                deletableFromAdapter.deleteItem(position);
            }
        });

        builder.setNegativeButton(view.getContext().getString(R.string.confirm_no_button), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public static class PurchaseViewHolder extends RecyclerView.ViewHolder {
        public TextView buyerName;
        public TextView consumersCount;
        public TextView amount;
        public TextView description;
        public TextView date;
        public ImageButton deleteButton;
        public View view;

        public PurchaseViewHolder(View v) {
            super(v);
            view = v;
            buyerName = (TextView) v.findViewById(R.id.purchase_list_item_buyer);
            consumersCount = (TextView) v.findViewById(R.id.purchase_list_item_consumers_count);
            amount = (TextView) v.findViewById(R.id.purchaser_list_item_amount);
            description = (TextView) v.findViewById(R.id.purchase_list_item_description);
            date = (TextView) v.findViewById(R.id.purchase_list_item_date);
            deleteButton = (ImageButton) v.findViewById(R.id.purchase_list_delete_item_button);
        }
    }

    public static class PurchaseItemContainer {
        public PurchaseItemContainer() {
        }

        public Integer id;
        public Integer eventId;
        public Float amount;
        public String description;
        public String buyerName;
        public Integer consumersCount;
        public String date;
    }
}
