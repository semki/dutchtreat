package net.de1mos.dutchtreat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by denis on 20.08.16.
 */
public class Utils {

    public static String getTSFromDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return df.format(date);
    }
}
