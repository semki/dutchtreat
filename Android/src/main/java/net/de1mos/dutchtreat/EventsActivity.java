package net.de1mos.dutchtreat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.de1mos.dutchtreat.dto.Evento;
import net.de1mos.dutchtreat.gateway.ConnectionFactory;
import net.de1mos.dutchtreat.gateway.UserCredentialsDAO;

import java.util.ArrayList;
import java.util.List;

public class EventsActivity extends AppCompatActivity {

    //TextView mUsernameText;

    private ListView mEventsList;

    private List<Evento> events;

    private final Integer LOGIN_ACITVITY_REQUEST_CODE = 123;
    private View mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Toolbar toolbar = (Toolbar) findViewById(R.id.events_toolbar);
        setSupportActionBar(toolbar);

        ConnectionFactory.SERVER_URL = getString(R.string.server_url);

        //mUsernameText = (TextView) findViewById(R.id.username_text);
        mEventsList = (ListView) findViewById(R.id.events_list);
        mProgressView = findViewById(R.id.events_progress);

        mEventsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent eventDetailsActivityIntent = new Intent(EventsActivity.this, EventDetailsActivity.class);
                eventDetailsActivityIntent.putExtra("eventId", events.get(i).getId());
                startActivity(eventDetailsActivityIntent);
            }
        });
        checkLogin();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_settings:
                return true;
            case R.id.nav_logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {
        UserCredentialsDAO dao = new UserCredentialsDAO();
        dao.removeCredentials();
        checkLogin();
    }

    private void checkLogin() {
        if (ConnectionFactory.notLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, LOGIN_ACITVITY_REQUEST_CODE);
        }
        else {
            refreshServerData();
        }
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {
        if (LOGIN_ACITVITY_REQUEST_CODE.equals(requestCode)) {
            refreshServerData();
        }
    }

    private void refreshServerData() {
        getEvents();
        getUsername();
    }

    private void getUsername() {
        new GetUsernameTask().execute();
    }

    private void getEvents() {
        showProgress(true);
        new GetEventsListTask().execute();
    }

    private class GetUsernameTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {

            try {
                return ConnectionFactory.getUsername();
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }

        @Override
        protected void onPostExecute(final String username) {
            // TODO find a good place
            //mUsernameText.setText(username);
        }
    }

    private class GetEventsListTask extends AsyncTask<Void, Void, List<Evento> > {
        @Override
        protected List<Evento> doInBackground(Void... voids) {
            try {
                return ConnectionFactory.getEventos();
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }

        @Override
        protected void onPostExecute(List<Evento> eventos) {
            showProgress(false);
            EventsActivity.this.events = eventos;
            List<String> eventsNames = new ArrayList<>();
            for (Evento evento : eventos) {
                eventsNames.add(evento.getName());
            }

            ArrayAdapter<String> adapter =
                    new ArrayAdapter<>(EventsActivity.this, R.layout.support_simple_spinner_dropdown_item, eventsNames);

            mEventsList.setAdapter(adapter);
        }

        @Override
        protected void onCancelled() {
            showProgress(false);
            super.onCancelled();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mEventsList.setVisibility(show ? View.GONE : View.VISIBLE);
            mEventsList.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mEventsList.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mEventsList.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
