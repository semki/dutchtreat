package net.de1mos.dutchtreat.gateway;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import net.de1mos.dutchtreat.dto.BalanceSummaryDTO;
import net.de1mos.dutchtreat.dto.Evento;
import net.de1mos.dutchtreat.dto.Purchase;
import net.de1mos.dutchtreat.dto.Transfer;
import net.de1mos.dutchtreat.dto.User;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by denis on 05.07.16.
 */
public class ConnectionFactory {

    private static final String SERVER_LOGIN_URL = "/dutch-treat/app/api/auth/login";

    public static String SERVER_URL; // = "http://semki.de1mos.net"; //"http://192.168.25.43:8080";

    private static final String SERVER_CURRENT_USER_URL = "/dutch-treat/app/api/auth/current";

    private static final String SERVER_GET_EVENTOS_URL = "/dutch-treat/app/api/eventos";

    private static final String SERVER_GET_TRANSFERS_URL = "/dutch-treat/app/api/transfers?event_id=%s";

    private static final String SERVER_CREATE_TRANSFERS_URL = "/dutch-treat/app/api/transfers";

    private static final String SERVER_GET_PURCHASES_URL = "/dutch-treat/app/api/purchases?event_id=%s";

    private static final String SERVER_GET_EVENTO_DETAILS_URL = "/dutch-treat/app/api/eventos/%s";

    private static final String SERVER_CREATE_PURCHASE_URL = "/dutch-treat/app/api/purchases";

    private static final String SERVER_DELETE_PURCHASE_URL = "/dutch-treat/app/api/purchases/%s";

    private static final String SERVER_DELETE_TRANSFER_URL = "/dutch-treat/app/api/transfers/%s";

    private static final String SERVER_GET_BALANCE_URL = "/dutch-treat/app/api/balance/summary-by-event/%s";

    private static final String COOKIES_HEADER = "Set-Cookie";

    private static final CookieManager msCookieManager = new CookieManager();

    private static User currentUser = null;

    public static boolean notLoggedIn() {
        return msCookieManager.getCookieStore().getCookies().size() == 0;
    }

    public static boolean login(String username, String password) {
        URL url;
        String credentials = String.format("username=%s&password=%s", username, password);

        try {
            url = new URL(getServerUrl() + SERVER_LOGIN_URL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(credentials);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                Map<String, List<String>> headerFields = conn.getHeaderFields();
                List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

                if (cookiesHeader != null) {
                    for (String cookie : cookiesHeader) {
                        msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                    }
                }

                currentUser = getUser();

                return true;
            } else {
                return false;

            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @NonNull
    private static String getServerUrl() {
        return SERVER_URL;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static String getUsername() {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + SERVER_CURRENT_USER_URL);

            RestTemplate template = getRestTemplate();
            User user = template.getForObject(uri, User.class);
            return user.getUserLogin();

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    private static User getUser() {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + SERVER_CURRENT_USER_URL);

            RestTemplate template = getRestTemplate();
            return template.getForObject(uri, User.class);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static List<Evento> getEventos() {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + SERVER_GET_EVENTOS_URL);

            RestTemplate template = getRestTemplate();
            Evento[] eventos = template.getForObject(uri, Evento[].class);
            return Arrays.asList(eventos);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static List<Transfer> getTransfers(String id) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + String.format(SERVER_GET_TRANSFERS_URL, id));
            RestTemplate template = getRestTemplate();
            Transfer[] transfers = template.getForObject(uri, Transfer[].class);
            return Arrays.asList(transfers);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static List<Purchase> getPurchases(String id) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + String.format(SERVER_GET_PURCHASES_URL, id));
            RestTemplate template = getRestTemplate();
            Purchase[] purchases= template.getForObject(uri, Purchase[].class);
            return Arrays.asList(purchases);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static Evento getEventoDetails(String id) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + String.format(SERVER_GET_EVENTO_DETAILS_URL, id));
            RestTemplate template = getRestTemplate();
            return template.getForObject(uri, Evento.class);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static void createTransfer(Transfer transfer) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + SERVER_CREATE_TRANSFERS_URL);
            RestTemplate template = getRestTemplate();
            template.postForLocation(uri, transfer);


        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }


    private static void checkLogin() {
        if (notLoggedIn()) {
            throw new RuntimeException("Not authorized");
        }
    }

    @NonNull
    private static RestTemplate getRestTemplate() {
        RestTemplate template = new RestTemplate();
        template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        template.setRequestFactory(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                super.prepareConnection(connection, httpMethod);
                connection.setRequestProperty("Cookie",
                        TextUtils.join(";",  msCookieManager.getCookieStore().getCookies()));
            }
        });
        return template;
    }

    public static void createPurchase(Purchase purchase) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + SERVER_CREATE_PURCHASE_URL);
            RestTemplate template = getRestTemplate();
            template.postForLocation(uri, purchase);


        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static void deletePurchase(Integer id) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + String.format(SERVER_DELETE_PURCHASE_URL, id));
            RestTemplate template = getRestTemplate();

            template.delete(uri);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static void deleteTransfer(Integer id) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + String.format(SERVER_DELETE_TRANSFER_URL, id));
            RestTemplate template = getRestTemplate();

            template.delete(uri);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }

    public static BalanceSummaryDTO getBalance(String id) {
        checkLogin();

        try {
            URI uri = new URI(getServerUrl() + String.format(SERVER_GET_BALANCE_URL, id));
            RestTemplate template = getRestTemplate();
            return template.getForObject(uri, BalanceSummaryDTO.class);

        } catch (Exception e) {
            throw new RuntimeException("Request error: ", e);
        }
    }
}

