package net.de1mos.dutchtreat.gateway;

import com.activeandroid.query.Select;

import net.de1mos.dutchtreat.entities.UserCredentials;

/**
 * Created by denis on 08.08.16.
 */
public class UserCredentialsDAO {

    public void setCredentials(UserCredentials credentials) {
        credentials.save();
    }

    public UserCredentials getCredentials() {
        return new Select().from(UserCredentials.class).executeSingle();
    }

    public void removeCredentials() {
        UserCredentials credentials = getCredentials();
        if (credentials != null) {
            credentials.delete();
        }
    }
}
