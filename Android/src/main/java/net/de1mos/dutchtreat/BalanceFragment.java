package net.de1mos.dutchtreat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.de1mos.dutchtreat.dto.BalanceRow;
import net.de1mos.dutchtreat.dto.BalanceSummaryDTO;
import net.de1mos.dutchtreat.gateway.ConnectionFactory;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

public class BalanceFragment extends Fragment {

    private Integer eventId;
    private View mProgressView;
    private ListView mBalanceList;
    private BalanceSummaryDTO balance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        eventId = getArguments().getInt(Constants.EVENT_ID, -1);
        View view = inflater.inflate(R.layout.fragment_balance, container, false);

        mProgressView = view.findViewById(R.id.balance_progress);
        mBalanceList = (ListView) view.findViewById(R.id.balance_list);

        getBalance();

        return view;
    }

    private void getBalance() {
        showProgress(true);
        new GetBalance().execute();
    }

     /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mBalanceList.setVisibility(show ? View.GONE : View.VISIBLE);
            mBalanceList.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mBalanceList.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mBalanceList.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }



    private class GetBalance extends AsyncTask<Long, Void, BalanceSummaryDTO > {

        @Override
        protected BalanceSummaryDTO doInBackground(Long... longs) {
            return ConnectionFactory.getBalance(String.valueOf(eventId));
        }

        @Override
        protected void onPostExecute(BalanceSummaryDTO balance) {
            showProgress(false);
            BalanceFragment.this.balance = balance;

            List<String> rowsTexts = new ArrayList<>();
            for (BalanceRow row : balance.getBalanceRows()) {
                rowsTexts.add(String.format(Locale.getDefault(), "%s:    %.2f %s", row.getParticipant().getName(), row.getAmount(), Currency.getInstance(Locale.getDefault()).getSymbol()));
            }

            ArrayAdapter<String> adapter =
                    new ArrayAdapter<>(BalanceFragment.this.getContext(), R.layout.support_simple_spinner_dropdown_item, rowsTexts);

            mBalanceList.setAdapter(adapter);
        }

        @Override
        protected void onCancelled() {
            showProgress(false);
            super.onCancelled();
        }
    }
}
