package net.de1mos.dutchtreat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.de1mos.dutchtreat.adapters.TrasferCardAdapter;
import net.de1mos.dutchtreat.dto.Transfer;
import net.de1mos.dutchtreat.gateway.ConnectionFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denis on 16.08.16.
 */
public class TransfersFragment extends Fragment implements DeletableFromAdapter {

    private static final int CREATE_TRANSFER_REQUEST_CODE = 42;
    private Integer eventId;
    private View mProgressView;
    private RecyclerView transfersList;
    private List<Transfer> transfers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        eventId = getArguments().getInt(Constants.EVENT_ID, -1);
        View view = inflater.inflate(R.layout.fragment_transfers, container, false);

        mProgressView = view.findViewById(R.id.transfers_progress);

        transfersList = (RecyclerView) view.findViewById(R.id.transfers_list);
        transfersList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        transfersList.setAdapter(new TrasferCardAdapter(new ArrayList<TrasferCardAdapter.TransferItemContainer>(), this));

        getTransfers();

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateTransferActivity.class);
                intent.putExtra(Constants.EVENT_ID, eventId);
                startActivityForResult(intent, CREATE_TRANSFER_REQUEST_CODE);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CREATE_TRANSFER_REQUEST_CODE) {
            getTransfers();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getTransfers() {
        showProgress(true);
        new GetTransfersList().execute();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            transfersList.setVisibility(show ? View.GONE : View.VISIBLE);
            transfersList.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    transfersList.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            transfersList.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void deleteItem(int position) {
        final Transfer transfer = transfers.get(position);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ConnectionFactory.deleteTransfer(transfer.getId());
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                getTransfers();
            }
        }.execute();
    }

    private class GetTransfersList extends AsyncTask<Long, Void, List<Transfer> > {

        @Override
        protected List<Transfer> doInBackground(Long... longs) {
            return ConnectionFactory.getTransfers(String.valueOf(eventId));
        }

        @Override
        protected void onPostExecute(List<Transfer> transfers) {
            showProgress(false);
            TransfersFragment.this.transfers = transfers;
            ArrayList<TrasferCardAdapter.TransferItemContainer> transferItemContainers = new ArrayList<>();
            for (Transfer transfer : transfers) {
                TrasferCardAdapter.TransferItemContainer container = new TrasferCardAdapter.TransferItemContainer();
                container.id = transfer.getId();
                container.amount = transfer.getAmount();
                container.description = transfer.getDescription();
                container.eventId = transfer.getEventId();
                container.receiverName = transfer.getReceiver().getName();
                container.senderName = transfer.getSender().getName();
                container.transferdate = transfer.getTransferdate();
                transferItemContainers.add(container);
            }
            //ArrayAdapter<String> adapter = new ArrayAdapter<String>(TransfersActivity.this, R.layout.support_simple_spinner_dropdown_item, transferNames);
            transfersList.setAdapter(new TrasferCardAdapter(transferItemContainers, TransfersFragment.this));
            transfersList.invalidate();
        }

        @Override
        protected void onCancelled() {
            showProgress(false);
            super.onCancelled();
        }


    }
}
