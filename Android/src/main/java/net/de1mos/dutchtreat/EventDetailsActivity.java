package net.de1mos.dutchtreat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by denis on 22.07.16.
 */
public class EventDetailsActivity extends AppCompatActivity {
    private int eventId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_evento_detail);

        eventId = getIntent().getIntExtra(Constants.EVENT_ID, -1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.events_toolbar);
        setSupportActionBar(toolbar);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.balance_tab_name)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.purchases_tab_name)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.transfers_tab_name)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final EventosPageAdapter adapter = new EventosPageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private class EventosPageAdapter extends FragmentPagerAdapter {

        public EventosPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.EVENT_ID, eventId);
            switch (position) {
                case 0:
                    BalanceFragment balanceFragment = new BalanceFragment();
                    balanceFragment.setArguments(bundle);
                    return balanceFragment;
                case 1:
                    PurchasesFragment purchasesFragment = new PurchasesFragment();
                    purchasesFragment.setArguments(bundle);
                    return purchasesFragment;
                case 2:
                    TransfersFragment transfersFragment = new TransfersFragment();
                    transfersFragment.setArguments(bundle);
                    return transfersFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
