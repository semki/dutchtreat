package net.de1mos.dutchtreat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.de1mos.dutchtreat.adapters.PurchaseCardAdapter;
import net.de1mos.dutchtreat.dto.Purchase;
import net.de1mos.dutchtreat.gateway.ConnectionFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denis on 16.08.16.
 */
public class PurchasesFragment extends Fragment implements DeletableFromAdapter{
    private static final int CREATE_PURCHASE_ACTIVITY_RESULT = 43;
    private Integer eventId;
    private View mProgressView;
    private RecyclerView purchasesList;
    private List<Purchase> purchases;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        eventId = getArguments().getInt(Constants.EVENT_ID, -1);
        View view = inflater.inflate(R.layout.fragment_purchases, container, false);

        mProgressView = view.findViewById(R.id.purchases_progress);
        purchasesList = (RecyclerView) view.findViewById(R.id.purchases_list);
        purchasesList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        purchasesList.setAdapter(new PurchaseCardAdapter(new ArrayList<PurchaseCardAdapter.PurchaseItemContainer>(), this));

        getPurchases();

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CreatePurchaseActivity.class);
                intent.putExtra(Constants.EVENT_ID, eventId);
                startActivityForResult(intent, CREATE_PURCHASE_ACTIVITY_RESULT);
            }
        });

        return view;
    }

    private void getPurchases() {
        showProgress(true);
        new GetPurchasesList().execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CREATE_PURCHASE_ACTIVITY_RESULT) {
            getPurchases();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            purchasesList.setVisibility(show ? View.GONE : View.VISIBLE);
            purchasesList.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    purchasesList.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            purchasesList.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void deleteItem(int position) {
        Purchase purchase = purchases.get(position);
        final Integer purchaseId = purchase.getId();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ConnectionFactory.deletePurchase(purchaseId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                getPurchases();
            }
        }.execute();
    }

    private class GetPurchasesList extends AsyncTask<Long, Void, List<Purchase> > {

        @Override
        protected List<Purchase> doInBackground(Long... longs) {
            return ConnectionFactory.getPurchases(String.valueOf(eventId));
        }

        @Override
        protected void onPostExecute(List<Purchase> purchases) {
            showProgress(false);
            PurchasesFragment.this.purchases = purchases;
            List<PurchaseCardAdapter.PurchaseItemContainer> purchasesAdapterList = new ArrayList<>();
            for (Purchase purchase : purchases) {
                PurchaseCardAdapter.PurchaseItemContainer container = new PurchaseCardAdapter.PurchaseItemContainer();
                container.amount = purchase.getAmount();
                container.buyerName = purchase.getBuyer().getName();
                container.date = purchase.getDate();
                container.description = purchase.getDescription();
                container.consumersCount = purchase.getConsumers().size();
                container.id = purchase.getId();
                container.eventId = purchase.getEventId();
                purchasesAdapterList.add(container);
            }
            purchasesList.setAdapter(new PurchaseCardAdapter(purchasesAdapterList, PurchasesFragment.this));
            purchasesList.invalidate();
        }

        @Override
        protected void onCancelled() {
            showProgress(false);
            super.onCancelled();
        }
    }
}
