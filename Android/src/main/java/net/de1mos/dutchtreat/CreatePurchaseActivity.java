package net.de1mos.dutchtreat;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import net.de1mos.dutchtreat.dto.Evento;
import net.de1mos.dutchtreat.dto.Participant;
import net.de1mos.dutchtreat.dto.Purchase;
import net.de1mos.dutchtreat.dto.User;
import net.de1mos.dutchtreat.fragments.DatePickerFragment;
import net.de1mos.dutchtreat.gateway.ConnectionFactory;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreatePurchaseActivity extends AppCompatActivity {

    private Integer eventId;
    private EditText purchaseDateField;
    private Spinner buyerSpinner;
    private List<Participant> participants;
    private ListView consumersField;
    private User currentUser;
    private TextInputEditText mAmount;
    private EditText mDescription;
    private Date purchaseDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_purchase);

        eventId = getIntent().getIntExtra(Constants.EVENT_ID, -1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.events_toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(getString(R.string.create_purchase));
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        mAmount = (TextInputEditText) findViewById(R.id.amount);
        mDescription = (EditText) findViewById(R.id.description);
        buyerSpinner = (Spinner) findViewById(R.id.buyer);
        consumersField = (ListView) findViewById(R.id.consumers);
        consumersField.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        consumersField.setItemsCanFocus(false);


        currentUser = ConnectionFactory.getCurrentUser();
        purchaseDateField = (EditText) findViewById(R.id.purchase_date);
        purchaseDate = Calendar.getInstance().getTime();
        purchaseDateField.setText(DateFormat.getDateInstance().format(purchaseDate));

        new GetEventoDetails().execute();
    }

    public void onOpenPurhcaseDateCalendar(View view) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                cal.set(Calendar.MONTH, monthOfYear);
                purchaseDate = cal.getTime();
                String dateString = DateFormat.getDateInstance().format(purchaseDate);
                purchaseDateField.setText(dateString);
            }
        };
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void onSaveButtonClick(View view) {
        Purchase purchase = new Purchase();
        purchase.setAmount(Float.valueOf(mAmount.getText().toString()));
        purchase.setDescription(mDescription.getText().toString());
        purchase.setEventId(eventId);
        purchase.setBuyer(participants.get(buyerSpinner.getSelectedItemPosition()));
        purchase.setDate(Utils.getTSFromDate(purchaseDate));

        SparseBooleanArray checkedItemPositions = consumersField.getCheckedItemPositions();
        for (int i = 0; i < checkedItemPositions.size(); i++) {
            if (checkedItemPositions.valueAt(i)) {
                int key = checkedItemPositions.keyAt(i);
                purchase.getConsumers().add(participants.get(key));
            }
        }

        new CreatePurchaseAsync(purchase).execute();
        Toast.makeText(CreatePurchaseActivity.this, "Save clicked", Toast.LENGTH_SHORT).show();
    }



    private class GetEventoDetails extends AsyncTask<Long, Void, Evento> {

        @Override
        protected Evento doInBackground(Long... longs) {
            return ConnectionFactory.getEventoDetails(String.valueOf(eventId));
        }

        @Override
        protected void onPostExecute(Evento evento) {
            //showProgress(false);
            participants = evento.getParticipants();
            refreshParticipants();
        }

    }

    private class CreatePurchaseAsync extends AsyncTask<Purchase, Void, Void> {

        private final Purchase purchase;

        public CreatePurchaseAsync(Purchase purchase) {
            this.purchase = purchase;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            finish();
        }

        @Override
        protected Void doInBackground(Purchase... purchases) {
            ConnectionFactory.createPurchase(purchase);
            return null;
        }
    }

    private void refreshParticipants() {

        ArrayList<String> spinnerData = new ArrayList<>();
        int selectedIdx = 0;
        for (int i=0; i<participants.size(); i++) {
            Participant participant = participants.get(i);
            spinnerData.add(participant.getName());
            if (participant.getLinkedAccount() != null && participant.getLinkedAccount().getId().equals(currentUser.getId())) {
                selectedIdx = i;
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        buyerSpinner.setAdapter(adapter);
        buyerSpinner.setSelection(selectedIdx);


        List<String> consumers = new ArrayList<>();
        for (Participant participant : participants) {
            consumers.add(participant.getName());
        }

        ArrayAdapter<String> listAdapter =
                new ArrayAdapter<>(CreatePurchaseActivity.this, android.R.layout.simple_list_item_multiple_choice, consumers);

        consumersField.setAdapter(listAdapter);
        for (int i = 0; i<consumers.size(); i++) {
            consumersField.setItemChecked(i, true);
        }

        consumersField.invalidate();

        //receiverSpinner.setAdapter(adapter);
    }
}
