package net.de1mos.dutchtreat;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import net.de1mos.dutchtreat.dto.Evento;
import net.de1mos.dutchtreat.dto.Participant;
import net.de1mos.dutchtreat.dto.Transfer;
import net.de1mos.dutchtreat.dto.User;
import net.de1mos.dutchtreat.fragments.DatePickerFragment;
import net.de1mos.dutchtreat.gateway.ConnectionFactory;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateTransferActivity extends AppCompatActivity {

    private Integer eventId;
    private EditText transferDateField;
    private Spinner senderSpinner;
    private List<Participant> participants;
    private Spinner receiverSpinner;
    private User currentUser;
    private TextInputEditText mAmount;
    private EditText mDescription;
    private Date transferDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_transfer);

        eventId = getIntent().getIntExtra(Constants.EVENT_ID, -1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.events_toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(getString(R.string.create_transfer));
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        mAmount = (TextInputEditText) findViewById(R.id.amount);
        mDescription = (EditText) findViewById(R.id.description);
        senderSpinner = (Spinner) findViewById(R.id.sender);
        receiverSpinner = (Spinner) findViewById(R.id.receiver);

        currentUser = ConnectionFactory.getCurrentUser();
        transferDateField = (EditText) findViewById(R.id.transfer_date);
        transferDate = Calendar.getInstance().getTime();
        transferDateField.setText(DateFormat.getDateInstance().format(transferDate));

        new GetEventoDetails().execute();
    }

    public void onOpenTransferDateCalendar(View view) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                cal.set(Calendar.MONTH, monthOfYear);
                transferDate = cal.getTime();
                String dateString = DateFormat.getDateInstance().format(transferDate);
                transferDateField.setText(dateString);
            }
        };
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void onSaveButtonClick(View view) {
        Transfer t = new Transfer();
        t.setAmount(Float.valueOf(mAmount.getText().toString()));
        t.setDescription(mDescription.getText().toString());
        t.setEventId(eventId);
        t.setSender(participants.get(senderSpinner.getSelectedItemPosition()));
        t.setReceiver(participants.get(receiverSpinner.getSelectedItemPosition()));
        t.setTransferdate(Utils.getTSFromDate(transferDate));
        new CreateTransferAsync(t).execute();
        Toast.makeText(CreateTransferActivity.this, "Save clicked", Toast.LENGTH_SHORT).show();
    }



    private class GetEventoDetails extends AsyncTask<Long, Void, Evento> {

        @Override
        protected Evento doInBackground(Long... longs) {
            return ConnectionFactory.getEventoDetails(String.valueOf(eventId));
        }

        @Override
        protected void onPostExecute(Evento evento) {
            //showProgress(false);
            participants = evento.getParticipants();
            refreshParticipants();
        }

    }

    private class CreateTransferAsync extends AsyncTask<Transfer, Void, Void> {

        private final Transfer transfer;

        public  CreateTransferAsync(Transfer transfer) {
            this.transfer = transfer;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            finish();
        }

        @Override
        protected Void doInBackground(Transfer... transfers) {
            ConnectionFactory.createTransfer(transfer);
            return null;
        }
    }

    private void refreshParticipants() {

        ArrayList<String> spinnerData = new ArrayList<>();
        int selectedIdx = 0;
        for (int i=0; i<participants.size(); i++) {
            Participant participant = participants.get(i);
            spinnerData.add(participant.getName());
            if (participant.getLinkedAccount() != null && participant.getLinkedAccount().getId().equals(currentUser.getId())) {
                selectedIdx = i;
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        senderSpinner.setAdapter(adapter);
        senderSpinner.setSelection(selectedIdx);

        receiverSpinner.setAdapter(adapter);
    }
}
