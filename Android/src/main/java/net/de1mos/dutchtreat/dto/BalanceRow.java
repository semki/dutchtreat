
package net.de1mos.dutchtreat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "participant",
    "amount"
})
public class BalanceRow {

    @JsonProperty("participant")
    private Participant participant;
    @JsonProperty("amount")
    private Double amount;

    /**
     * 
     * @return
     *     The participant
     */
    @JsonProperty("participant")
    public Participant getParticipant() {
        return participant;
    }

    /**
     * 
     * @param participant
     *     The participant
     */
    @JsonProperty("participant")
    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    /**
     * 
     * @return
     *     The amount
     */
    @JsonProperty("amount")
    public Double getAmount() {
        return amount;
    }

    /**
     * 
     * @param amount
     *     The amount
     */
    @JsonProperty("amount")
    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
