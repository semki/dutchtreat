
package net.de1mos.dutchtreat.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "event_id",
    "amount",
    "description",
    "sender",
    "receiver",
    "transferdate"
})
public class Transfer {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("event_id")
    private Integer eventId;
    @JsonProperty("amount")
    private Float amount;
    @JsonProperty("description")
    private String description;
    @JsonProperty("sender")
    private Participant sender;
    @JsonProperty("receiver")
    private Participant receiver;
    @JsonProperty("transferdate")
    private String transferdate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The eventId
     */
    @JsonProperty("event_id")
    public Integer getEventId() {
        return eventId;
    }

    /**
     * 
     * @param eventId
     *     The event_id
     */
    @JsonProperty("event_id")
    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    /**
     * 
     * @return
     *     The amount
     */
    @JsonProperty("amount")
    public Float getAmount() {
        return amount;
    }

    /**
     * 
     * @param amount
     *     The amount
     */
    @JsonProperty("amount")
    public void setAmount(Float amount) {
        this.amount = amount;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The sender
     */
    @JsonProperty("sender")
    public Participant getSender() {
        return sender;
    }

    /**
     * 
     * @param sender
     *     The sender
     */
    @JsonProperty("sender")
    public void setSender(Participant sender) {
        this.sender = sender;
    }

    /**
     * 
     * @return
     *     The receiver
     */
    @JsonProperty("receiver")
    public Participant getReceiver() {
        return receiver;
    }

    /**
     * 
     * @param receiver
     *     The receiver
     */
    @JsonProperty("receiver")
    public void setReceiver(Participant receiver) {
        this.receiver = receiver;
    }

    /**
     * 
     * @return
     *     The transferdate
     */
    @JsonProperty("transferdate")
    public String getTransferdate() {
        return transferdate;
    }

    /**
     * 
     * @param transferdate
     *     The transferdate
     */
    @JsonProperty("transferdate")
    public void setTransferdate(String transferdate) {
        this.transferdate = transferdate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
