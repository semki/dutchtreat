package net.de1mos.dutchtreat.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by denis on 05.07.16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "user_login",
        "user_password",
        "password_confirmation",
        "email",
        "roles",
        "active"
})
public class User {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("user_login")
    private String userLogin;
    @JsonProperty("user_password")
    private Object userPassword;
    @JsonProperty("password_confirmation")
    private Object passwordConfirmation;
    @JsonProperty("email")
    private String email;
    @JsonProperty("roles")
    private List<Role> roles = new ArrayList<>();
    @JsonProperty("active")
    private Boolean active;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userLogin
     */
    @JsonProperty("user_login")
    public String getUserLogin() {
        return userLogin;
    }

    /**
     *
     * @param userLogin
     * The user_login
     */
    @JsonProperty("user_login")
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    /**
     *
     * @return
     * The userPassword
     */
    @JsonProperty("user_password")
    public Object getUserPassword() {
        return userPassword;
    }

    /**
     *
     * @param userPassword
     * The user_password
     */
    @JsonProperty("user_password")
    public void setUserPassword(Object userPassword) {
        this.userPassword = userPassword;
    }

    /**
     *
     * @return
     * The passwordConfirmation
     */
    @JsonProperty("password_confirmation")
    public Object getPasswordConfirmation() {
        return passwordConfirmation;
    }

    /**
     *
     * @param passwordConfirmation
     * The password_confirmation
     */
    @JsonProperty("password_confirmation")
    public void setPasswordConfirmation(Object passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    /**
     *
     * @return
     * The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The roles
     */
    @JsonProperty("roles")
    public List<Role> getRoles() {
        return roles;
    }

    /**
     *
     * @param roles
     * The roles
     */
    @JsonProperty("roles")
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    /**
     *
     * @return
     * The active
     */
    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * @param active
     * The active
     */
    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
