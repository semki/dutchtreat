
package net.de1mos.dutchtreat.dto;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "balanceRows"
})
public class BalanceSummaryDTO {

    @JsonProperty("balanceRows")
    private List<BalanceRow> balanceRows = new ArrayList<>();

    /**
     * 
     * @return
     *     The balanceRows
     */
    @JsonProperty("balanceRows")
    public List<BalanceRow> getBalanceRows() {
        return balanceRows;
    }

    /**
     * 
     * @param balanceRows
     *     The balanceRows
     */
    @JsonProperty("balanceRows")
    public void setBalanceRows(List<BalanceRow> balanceRows) {
        this.balanceRows = balanceRows;
    }

}
