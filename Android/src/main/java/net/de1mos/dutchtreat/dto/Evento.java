
package net.de1mos.dutchtreat.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "invate",
    "participants",
    "accessAccounts"
})
public class Evento {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("invate")
    private Object invate;
    @JsonProperty("participants")
    private List<Participant> participants = new ArrayList<>();
    @JsonProperty("accessAccounts")
    private List<AccessAccount> accessAccounts = new ArrayList<>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The invate
     */
    @JsonProperty("invate")
    public Object getInvate() {
        return invate;
    }

    /**
     * 
     * @param invate
     *     The invate
     */
    @JsonProperty("invate")
    public void setInvate(Object invate) {
        this.invate = invate;
    }

    /**
     * 
     * @return
     *     The participants
     */
    @JsonProperty("participants")
    public List<Participant> getParticipants() {
        return participants;
    }

    /**
     * 
     * @param participants
     *     The participants
     */
    @JsonProperty("participants")
    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    /**
     * 
     * @return
     *     The accessAccounts
     */
    @JsonProperty("accessAccounts")
    public List<AccessAccount> getAccessAccounts() {
        return accessAccounts;
    }

    /**
     * 
     * @param accessAccounts
     *     The accessAccounts
     */
    @JsonProperty("accessAccounts")
    public void setAccessAccounts(List<AccessAccount> accessAccounts) {
        this.accessAccounts = accessAccounts;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
