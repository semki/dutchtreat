
package net.de1mos.dutchtreat.dto;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "event_id",
    "amount",
    "description",
    "date",
    "buyer",
    "consumers"
})
public class Purchase {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("event_id")
    private Integer eventId;
    @JsonProperty("amount")
    private Float amount;
    @JsonProperty("description")
    private String description;
    @JsonProperty("date")
    private String date;
    @JsonProperty("buyer")
    private Participant buyer;
    @JsonProperty("consumers")
    private List<Participant> consumers = new ArrayList<>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The eventId
     */
    @JsonProperty("event_id")
    public Integer getEventId() {
        return eventId;
    }

    /**
     * 
     * @param eventId
     *     The event_id
     */
    @JsonProperty("event_id")
    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    /**
     * 
     * @return
     *     The amount
     */
    @JsonProperty("amount")
    public Float getAmount() {
        return amount;
    }

    /**
     * 
     * @param amount
     *     The amount
     */
    @JsonProperty("amount")
    public void setAmount(Float amount) {
        this.amount = amount;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The buyer
     */
    @JsonProperty("buyer")
    public Participant getBuyer() {
        return buyer;
    }

    /**
     * 
     * @param buyer
     *     The buyer
     */
    @JsonProperty("buyer")
    public void setBuyer(Participant buyer) {
        this.buyer = buyer;
    }

    /**
     * 
     * @return
     *     The consumers
     */
    @JsonProperty("consumers")
    public List<Participant> getConsumers() {
        return consumers;
    }

    /**
     * 
     * @param consumers
     *     The consumers
     */
    @JsonProperty("consumers")
    public void setConsumers(List<Participant> consumers) {
        this.consumers = consumers;
    }

}
