package net.de1mos.dutchtreat.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by denis on 08.08.16.
 */
@Table(name="user_credentials")
public class UserCredentials extends Model {

    public UserCredentials() {
    }

    public UserCredentials(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Column()
    public String login;

    @Column()
    public String password;
}
